﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motori
{
    public class Resenje
    {
        public Resenje()
        {
            Pcd = -1;
            ejd = -1;
            kd = -1;
            km = -1;
            sigmap = -1;
            gamas = -1;
            hdprim = -1;
            bdprim = -1;
            redni_broj_da = -1;
            redni_broj_Vk = -1;
            redni_broj_kolektor = -1;
            redni_broj_dp = -1;
            bi = -1;
            Dkprim = -1;
            k = -1;
            deltapprim = -1;
            sigma = -1;
            eta = -1;
            A = -1;
            Bs = -1;
            lambda = -1;
            Pt = -1;
            kp = -1;
            ka = -1;
            izabran_motor = -1;
        }
        public double ejd;
        public double Pcd;
        public double kd;
        public double km;
        public double sigmap;
        public double gamas;
        public double hdprim;
        public double bdprim;
        public int redni_broj_Vk;
        public int redni_broj_kolektor;
        public int redni_broj_da;
        public int redni_broj_dp;
        public double bi;
        public double Dkprim; 
        public double k;
        public double deltapprim;
        public double sigma;
        public double eta;
        public double A;
        public double Bs;
        public double lambda;
        public double Pt;
        public double kp;
        public double ka;
        public int izabran_motor;
    }
}
