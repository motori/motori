﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Motori
{
    public partial class Biranje_Motora : Form
    {
        private Motor[] motori;
        public Motor izabran;
        public bool closed = true;
        public Biranje_Motora(Motor[] m)
        {
            InitializeComponent();
            if (m.Length == 0) {
                throw new Exception("prazan niz u konstruktoru biranja motora!");
            }
            for (int i = 0; i < m.Length; i++)
                listBox1.Items.Add(m[i].getime());
            listBox1.SelectedIndex = 0;
            motori = m;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            closed = false;
            izabran = motori[listBox1.SelectedIndex];
            Dispose();
        }

    }
}
