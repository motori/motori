﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motori
{
    public class Motor
    {
        private const double PI = 3.1415926535896;
        private string ime;
        private double da;
        private double z;
        private double hz;
        private double az;
        private double ap;
        private double bz;
        private double sz;
        private double hkr;
        private double ds;
        private double dos;
        private double alpha;
        private double di;
        private double dss;
        private double hp;
        private double b;
        private double bs;
        private double bsmax;
        public Motor(string s,double a, double b1, double c, double d, double e, double f, double g, double h, double i, double j, double k, double l, double m, double n, double o, double p)
        {
            ime = s;
            da=a;
            z=b1;
            hz=c;
            az=d;
            ap=e;
            bz=f;
            sz=g;
            hkr=h;
            ds=i;
            dos=j;
            alpha=k;
            di=l;
            dss=m;
            hp=n;
            b=o;
            bs=p;
        }
        private double f1(double p)
        {
            return 1.6 * 0.95 * ds * p / (PI * da * 1.2);
        }
        private double f2()
        {
            return 1.6 * 0.93 * bz * z / (PI * da);
        }
        public void calculatebsmax(double p)
        {
            bsmax = Utilities.min(f1(p), f2());
        }

        public string getime()
        {
            return ime;
        }
        public double getda ()
        {
            return da;
        }
        public double getz()
        {
            return z;
        }
        public double gethz()
        {
            return hz;
        }
        public double getaz()
        {
            return az;
        }
        public double getap()
        {
            return ap;
        }
        public double getbz()
        {
            return bz;
        }
        public double getsz()
        {
            return sz;
        }
        public double gethkr()
        {
            return hkr;
        }
        public double getds()
        {
            return ds;
        }
        public double getdo()
        {
            return dos;
        }
        public double getalpha()
        {
            return alpha;
        }
        public double getdi()
        {
            return di;
        }
        public double getdss()
        {
            return dss;
        }
        public double gethp()
        {
            return hp;
        }
        public double getb()
        {
            return b;
        }
        public double getbs()
        {
            return bs;
        }
        public double getbsmax()
        {
            return bsmax;
        }
    }
}
