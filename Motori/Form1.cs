﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Motori
{

   

    public partial class Form1 : Form
    {
        private const int broj_seckanja_Dkprim = 4;
        private const int broj_seckanja_deltapprim = 4;
        private const int broj_seckanja_ejd = 30;
        private const int broj_seckanja_Pcd = 100;
        private const int broj_seckanja_eta = 30;
        private const int broj_seckanja_kd = 4;
        private const int broj_seckanja_gamas = 3;
        private const int broj_seckanja_sigmap = 3;
        private const int broj_seckanja_lambda = 20;
        private const int broj_seckanja_km = 4;
        private const int broj_seckanja_sigma = 2;
        private const int broj_seckanja_A = 20;
        private const int broj_seckanja_bi = 2;
        private const int broj_seckanja_bdprim = 2;
        private const int broj_seckanja_hdprim = 2;
        private const int broj_seckanja_k = 2;
        private const int broj_seckanja_Bs = 25;
        private const int broj_seckanja_Pt = 4;
        private const int broj_seckanja_ka = 5;
        private const int broj_seckanja_kp = 3;
        private double Scuaprim;
        private double bi_min;
        private double bi_max;
        private int izabran_da;
        private int izabran_Vk;
        private int izabran_dp;
        private double kn;
        private double mi;
        private double deltaU;
        private double deltacosfi;
        private double deltaUd;
        private double deltad;
        private double Pd;
        private double deltapprim_min;
        private double deltapprim_max;
        private double daprim;
        private double da;
        private double d;
        private double bprim;
        private double lprim;
        private double kprim;
        private double cprim;
        private double aprim;
        private double e;
        private double Scua;
        private double deltaa;
        private double Np;
        private double alfacu;
        private double Bz;
        private double Ha;
        private double lpr;
        private double Ra;
        private double deltaUa;
        private double ks;
        private double bzkr;
        private double Ms;
        private double ha;
        private double Ba;
        private double sigma;
        private double Mz;
        private double Ma;
        private double la;
        private double Hz;
        private const double sigma_min = 1.02;
        private const double sigma_max = 1.2;
        private const double lambda_min = 0.4;
        private const double lambda_max = 2;
        private const double PI = 3.1415926535896;
        private double ejd_max ;
        private double ejd_min ;
        private const double k1 = 1000;
        private const double kp_min = 1.2;
        private const double kp_max = 2; 
        private const double ka_min = 0.8;
        private const double ka_max = 1.3;
        private const int BROJ_MOTORA = 13;
        private bool suspend = false;
        private double Va;
        private double kp;
        private short levidesni;
        private double Bp;
        private double bp;
        private double Mp;
        private double lj;
        private double Hp;
        private double Bj;
        private double Hj;
        private double Mj;
        private double Fp;
        private double k;
        private double Npprim;
        private double Npsekund;
        private double Scupprim;
        private double deltapprim;
        private double dp;
        private double dpprim;
        private double Scup;
        private double Rp;
        private double lsrp;
        private double kteta;
        private double deltaUp;
        private double Dkprim;
        private double Dk;
        private double tk;
        private double bi;
        private double bk;
        private double Pcd_min;
        private double Pcd_max;
        private double Pcd;
        private double Vkprim;
        private double Vk;
        private double Sdprim;
        private double bdprim;
        private double bdprim_min;
        private double bdprim_max;
        private double adprim;
        private double ad;
        private double bd;
        private double hd;
        private double hdprim;
        private double hdprim_min;
        private double hdprim_max;
        private double deltadprim;
        private double bkz;
        private double y1;
        private double y2;
        private double bkprim;
        private double tkprim;
        private double lambdagama;
        private double ek;
        private double lambdaq;
        private double eq;
        private double er;
        private double et;
        private int redni_broj_da;
        private int redni_broj_dp;
        private int redni_broj_Vk;
        private int redni_broj_kolektor;
        private double deltaUaprim;
        private double gamas;
        private double gamas_min=0.75;
        private double gamas_max=0.85;
        private double sigmap;
        private double sigmap_min=1.08;
        private double sigmap_max=1.12;
        private double xagamaI;
        private double xpgamaI;
        private double deltaUrprim;
        private double Ep;
        private double Eq;
        private double Ua;
        private double Ur;
        private double Um;
        private double cosfiprim;
        private double Pcua;
        private double Pcup;
        private double Pdk;
        private double Gp;
        private double Gja;
        private double Gza;
        private double Gjs;
        private double Pp;
        private double pp;
        private double ps;
        private double pz;
        private double pa;
        private double Pja;
        private double Pza;
        private double Pjs;
        private double Pfe;
        private double Ptrv;
        private double Pmeh;
        private double km;
        private double km_min=1;
        private double km_max=3;
        private double Pgama;
        private double kd;
        private double kd_max=1.2;
        private double kd_min=1.08;
        private double etaprim;
        private double deltaeta;
        private double Ptrd;
        private double Ptrl;
        private double ka;
        private double deltaaprim;
        private double deltaaprim_min;
        private double deltaaprim_max;
        private double Teta;
        private double Tau;
        private double lp;
        private double fa;
        private double Pprim;
        private double U;
        private double f;
        private double Q;
        private double Daprim;
        private double n;
        private double p;
        private double P;
        private double Pa;
        private double Ea;
        private double eta;
        private double Bs;
        private double A;
        private double Pt;
        private double Pt_min;
        private double Pt_max;
        private double Bs_min;
        private double A_min;
        private double Bs_max;
        private double A_max;
        private double c;
        private double alfa;
        private double lambda;
        private double cosfi;
        private double I;
        private double Fa;
        private double m;
        private double a;
        private double A2;
        private double deltaA;
        private double deltaA2;
        private double Naprim;
        private double N1prim;
        private double Nzprim;
        private double N1;
        private double Nz;
        private double Na;
        private double[] Bpi;
        private double[] Bzi;
        private double eta_max;
        private double eta_min;
        private Thread radnik;
        private double A3;
        private Resenje[] resenja;
        private int izabran_kolektor;
        private int izabran_motor;
        private Motor[] motori;
        private Cetkica[] cetkice;
        private Kolektor[] kolektori;
        private double[] vrednostida;
        private double[] nizhdadbd;
        public Form1()
        {
            InitializeComponent();
            motori = new Motor[BROJ_MOTORA];
            motori[0]= new Motor("GA 8046",44.6,12,10.2,7.68,2.1,2.5,56,0.5,45.5,15.5,120,72,80,6.37,24.423,7.433);
            motori[1]= new Motor("GA 81466",45.55,14,10.23,6.96,2.32,2.2,54.95,1.13,46.6,15.1,125.5,74.62,81,7.626,22.775,6.249);
            motori[2]= new Motor("GA 9048",48.5,12,12,8.6,3.55,2.21,73.4,0.85,49,11,123.3,69,90,1.054,39.53,7.38);
            motori[3]= new Motor("GA 9351",52.4,16,11.55,6.876,2.52,2.6,53.8,0.9,53.2,16,127.9,88.2,93.7,5.29,28.79,7.5159);
            motori[4]= new Motor("GA 9958",57.5,18,10.25,6.04,2.3,2.55,44.8,0.91,58.5,20,125,78,100,2.121,43.496,8.593);
            motori[5]= new Motor("GA 1264",64.15,13,15,10.1,2.6,4.029,105.3,0.7,65,16,124.2,90,120,1.272,53.0165,9.5429);
            motori[6]= new Motor("GA 3821",21.5,8,6,4.92,2,1.75,19.1,0.85,22,5.15,117.67,32,38,1.15,16.78,2.62);
            motori[7]= new Motor("GA 4325",24.4,8,5.6,6.38,2.4,1.5,27.2,0.5,25.2,8.03,110.22,39,43.5,4.24,12.2,4.24);
            motori[8]= new Motor("GA 5431",31,12,8,4.73,2,2,24.3,0.9,31.7,9,119.2,45,54.02,1.786,25.94,3.257);
            motori[9]= new Motor("GA 5834b",33.4,14,8.9,5.14,2.2,1.7,30,0.5,34.4,8.97,107.2,52,58,3.98,11.15,3.185);
            motori[10]= new Motor("GA 5834",35,12,8.5,5.6,1.8,2,32.8,0.9,35.3,10,108.7,46,58.5,1.27,25.77,4.348);
            motori[11]= new Motor("GA 58347",35,12,8.25,5.62,2.22,2,33.3,0.66,35.3,8.25,107.88,46,58.5,1.27,25.77,4.348);
            motori[12]= new Motor("GA 7341",40.2,13,9.6,6.68,2.05,2,43.1,0.9,41,12,121.3,53,73,1.057,34.91,5.289);

            vrednostida = new Double[44] {0.106,0.112,0.118,0.125,0.132,0.14,0.15,0.16,0.17,0.18,0.19,0.20,0.212,0.224,0.236,0.25,0.265,0.28,0.3,0.315,0.335,0.355,0.375,0.4,0.425,0.45,0.475,0.5,0.53,0.56,0.6,0.63,0.67,0.71,0.75,0.8,0.85,0.9,0.95,1,1.06,1.12,1.18,1.25};

            nizhdadbd = new Double[18] {1.6,2,2.5,4,5,6.3,8,10,12.5,16,20,25,32,40,50,64,80,125 };

            kolektori = new Kolektor[28];

            

            kolektori[0] = new Kolektor(27,27,10,16,16,0,0,0);
            kolektori[1] = new Kolektor(27,27,10,16,16,0,0,0);
            kolektori[2] = new Kolektor(27,27,9.5,13,13,0,0,0);
            kolektori[3] = new Kolektor(27,26,8,13,13,2,0,0);
            kolektori[4] = new Kolektor(25,25,10,13,13,0,0,0);
            kolektori[5] = new Kolektor(28,28,9,16.1,15,0,0,1);
            kolektori[6] = new Kolektor(26,28,10,16,16,0,0,0);
            kolektori[7] = new Kolektor(28,28,9,16.1,15,0,0,1.1);
            kolektori[8] = new Kolektor(28.6,28.6,11,17.5,14.5,0,1.5,1.5);
            kolektori[9] = new Kolektor(30,30,8,12,12,0,0,0);
            kolektori[10] = new Kolektor(30,29,13,19,16.2,2.5,2.5,0.3);
            kolektori[11] = new Kolektor(30,29.2,11,20,17.5,3.5,1,1.5);
            kolektori[12] = new Kolektor(30,30,10,15,15,0,0,0);
            kolektori[13] = new Kolektor(29.5,29.5,10,19,16,0,1.5,1.5);
            kolektori[14] = new Kolektor(29.5,28,12,14.5,14,4,0,0.5);
            kolektori[15] = new Kolektor(29.5,28,10.2,14.5,14,4,0,0.5);
            kolektori[16] = new Kolektor(29,28,10,15,15,3,0,0);
            kolektori[17] = new Kolektor(30,29.2,11,20,17.5,3.5,1,1.5);
            kolektori[18] = new Kolektor(31,31,10,17.5,17.5,0,0,0);
            kolektori[19] = new Kolektor(31,31,11.2,17.5,17.5,0,0,0);
            kolektori[20] = new Kolektor(30.8,30.8,11.2,20,19,0,1,0);
            kolektori[21] = new Kolektor(36,31.7,12,24.5,21,4.5,2.5,1);
            kolektori[22] = new Kolektor(36,31.7,12,24.5,21,0,2.5,1);
            kolektori[23] = new Kolektor(39.5,36,15.2,23,23,5,0,0);
            kolektori[24] = new Kolektor(38.5,35.5,15,23,23,3,0,0);
            kolektori[25] = new Kolektor(39.5,36,15.2,25,25,5,0,0);
            kolektori[26] = new Kolektor(37,36,15.2,23,23,3,0,0);
            kolektori[27] = new Kolektor(66,60.4,18.6,36.7,32.5,3.6,4.2,0);

            cetkice = new Cetkica[36];

            cetkice[0] = new Cetkica("UGLJENA","U1T",60,6000,1.45,200,0.2,3.5,6,15);
            cetkice[1] = new Cetkica("UGLJENA","U12",70,7500,1.5,300,0.3,3,6,20);
            cetkice[2] = new Cetkica("GRAFITNA","G1U",15,1500,1.6,150,0.2,2.5,8,45);
            cetkice[3] = new Cetkica("GRAFITNA", "G11", 180, 4500, 1.9, 280, 0.08, 2.9, 7, 40);
            cetkice[4] = new Cetkica("GRAFITNA", "EGG1", 15, 1100, 1.6, 150, 0.15, 2, 10, 60);
            cetkice[5] = new Cetkica("GRAFITNA", "EGG2", 20, 850, 1.62, 180, 0.15, 2, 10, 75);
            cetkice[6] = new Cetkica("BAKELIT GRAFITNA", "GB1", 20, 8000, 1.65, 400, 0.15, 2.2, 7, 30);
            cetkice[7] = new Cetkica("BAKELIT GRAFITNA", "GB2", 25, 30000, 1.5, 350, 0.15, 2.6, 6, 35);
            cetkice[8] = new Cetkica("BAKELIT GRAFITNA", "GB3", 20, 100000, 1.45, 250, 0.12, 3.2, 4, 40);
            cetkice[9] = new Cetkica("ELEKTROGRAFITIRANA","EG01",30,1000,1.6,170,0.12,2.8,12,60);
            cetkice[10] = new Cetkica("ELEKTROGRAFITIRANA", "EG01/U", 30, 1000, 1.6, 200, 0.1, 2.9, 12, 50);
            cetkice[11] = new Cetkica("ELEKTROGRAFITIRANA", "EG03", 35, 1200, 1.6, 170, 0.12, 2.8, 12, 40);
            cetkice[12] = new Cetkica("ELEKTROGRAFITIRANA", "EG04", 35, 1500, 1.6, 200, 0.12, 2.6, 12, 30);
            cetkice[13] = new Cetkica("ELEKTROGRAFITIRANA", "EG10", 45, 2000, 1.65, 200, 0.15, 2.8, 12, 40);
            cetkice[14] = new Cetkica("ELEKTROGRAFITIRANA", "EG10P", 40, 2000, 1.6, 200, 0.12, 3, 12, 50);
            cetkice[15] = new Cetkica("ELEKTROGRAFITIRANA", "EG11", 45, 2500, 1.65, 220, 0.15, 2.9, 12, 35);
            cetkice[16] = new Cetkica("ELEKTROGRAFITIRANA", "EG20", 45, 3000, 1.7, 220, 0.18, 3, 12, 50);
            cetkice[17] = new Cetkica("ELEKTROGRAFITIRANA", "EG21", 55, 3500, 1.7, 250, 0.18, 3.1, 12, 60);
            cetkice[18] = new Cetkica("ELEKTROGRAFITIRANA", "EG21B", 50, 3600, 1.72, 300, 0.2, 3.1, 12, 50);
            cetkice[19] = new Cetkica("ELEKTROGRAFITIRANA", "EG30", 35, 4000, 1.55, 200, 0.18, 2.5, 12, 35);
            cetkice[20] = new Cetkica("ELEKTROGRAFITIRANA", "EG31", 0, 4500, 1.55, 200, 0.2, 2.5, 12, 40);
            cetkice[21] = new Cetkica("ELEKTROGRAFITIRANA", "EG32", 45, 5000, 1.6, 250, 0.2, 2.6, 12, 45);
            cetkice[22] = new Cetkica("ELEKTROGRAFITIRANA", "EG32B", 55, 5000, 1.65, 300, 0.15, 2.7, 12, 45);
            cetkice[23] = new Cetkica("ELEKTROGRAFITIRANA", "EG33", 50, 5000, 1.65, 250, 0.2, 2.65, 12, 45);
            cetkice[24] = new Cetkica("ELEKTROGRAFITIRANA", "EG34", 25, 3300, 1.45, 230, 0.15, 2.5, 12, 60);
            cetkice[25] = new Cetkica("ELEKTROGRAFITIRANA", "EG44", 45, 5500, 1.43, 200, 0.15, 2.6, 12, 60);
            cetkice[26] = new Cetkica("ELEKTROGRAFITIRANA", "EG45U", 40, 6000, 1.35, 150, 0.12, 2.4, 12, 60);
            cetkice[27] = new Cetkica("METALO GRAFITNA", "BG30", 8, 1500, 2.5, 220, 0.1, 1, 10, 40);
            cetkice[28] = new Cetkica("METALO GRAFITNA", "BG50", 10, 1000, 3, 250, 0.12, 0.9, 12, 40);
            cetkice[29] = new Cetkica("METALO GRAFITNA", "BG65", 15, 250, 3.5, 300, 0.15, 0.8, 15, 35);
            cetkice[30] = new Cetkica("METALO GRAFITNA", "BG75", 13, 75, 4.5, 250, 0.18, 0.5, 18, 40);
            cetkice[31] = new Cetkica("METALO GRAFITNA", "BG82", 10, 12, 4.8, 300, 0.2, 0.3, 20, 35);
            cetkice[32] = new Cetkica("METALO GRAFITNA", "BG88", 16, 8, 5.4, 400, 0.2, 0.2, 25, 30);
            cetkice[33] = new Cetkica("METALO GRAFITNA", "BG92", 21, 5, 5.6, 500, 0.2, 0.15, 30, 25);
            cetkice[34] = new Cetkica("METALO GRAFITNA", "BZG1", 10, 30, 5.8, 1000, 0.15, 0.2, 20, 25);
            cetkice[35] = new Cetkica("METALO GRAFITNA", "BZG2", 12, 5, 6.6, 900, 0.15, 0.2, 23, 20);


            
            for (int i = 0; i < 28; i++)
                for (int j = i + 1; j < 28;j++ )
                    if (kolektori[i].D > kolektori[j].D)
                    {
                        Kolektor t;
                        t = kolektori[i];
                        kolektori[i] = kolektori[j];
                        kolektori[j] = t;
                    }
            for (int i = 0; i < 36; i++)
                for (int j = i + 1; j < 36; j++)
                    if (cetkice[i].Vk > cetkice[j].Vk)
                    {
                        Cetkica t;
                        t = cetkice[i];
                        cetkice[i] = cetkice[j];
                        cetkice[j] = t;
                    }

            Bpi = new Double[BROJ_MOTORA];
            Bzi = new Double[BROJ_MOTORA];
            resenja = new Resenje[BROJ_MOTORA];
            for (int i = 0; i < BROJ_MOTORA; i++)
            {
                resenja[i] = new Resenje();
            }
        }

        

        private void postavi_resenje(Resenje a)
        {
            izabran_motor = a.izabran_motor;
            ejd = a.ejd;
            Pcd = a.Pcd;
            km = a.km;
            kd = a.kd;
            sigmap = a.sigmap;
            gamas = a.gamas;
            hdprim = a.hdprim;
            bdprim = a.bdprim;
            redni_broj_Vk = a.redni_broj_Vk;
            redni_broj_kolektor = a.redni_broj_kolektor;
            redni_broj_da = a.redni_broj_da;
            redni_broj_dp = a.redni_broj_dp;
            bi = a.bi;
            Dkprim = a.Dkprim;
            deltapprim = a.deltapprim;
            k = a.k;
            sigma = a.sigma;
            eta = a.eta;
            A = a.A;
            Bs = a.Bs;
            lambda = a.lambda;
            Pt = a.Pt;
            kp = a.kp;
            ka = a.ka;
        }

        private int izracunaj_broj_motora()
        {
            int cnt=0;

            for (int i = 0; i < BROJ_MOTORA; i++)
            {
                if (resenja[i].Bs != -1)
                {
                    cnt++;
                }
            }
            return cnt;
        }


        private void izaberi_motor(int cnt)
        {
            Motor[] a = new Motor[cnt];
            cnt = 0;

            for (int i = 0; i < BROJ_MOTORA; i++)
            {
                postavi_resenje(resenja[i]);

                if (resenja[i].Bs != -1)
                {
                    a[cnt]=motori[i];
                    cnt++;
                }
            }

            Biranje_Motora biranje = new Biranje_Motora(a);

            biranje.ShowDialog();

            if (biranje.closed) Dispose();
            else 
            for (int i = 0; i < BROJ_MOTORA; i++)
            {
                if (biranje.izabran.getime().Equals(motori[i].getime()))
                {
                    izabran_motor = i;
                    postavi_resenje(resenja[i]);
                }

            }
        }


        private void izracunaj_a3()
        {
            A3 = 60 * Pa * 1000000000 / (n * lambda * (motori[izabran_motor].getda() ) * (motori[izabran_motor].getda() * motori[izabran_motor].getda() ) * PI * PI * alfa * Bs);
        }


        private void izracunaj_specificnu_zapreminu_motora()
        {
            alfa = motori[izabran_motor].getalpha() / 180;
            c = 60 / (alfa * PI * PI * A * Bs);
            
        }

        private void izracunaj_precnik_i_osnu_duzinu_rotora()
        { 
            Daprim= Math.Pow(c*Pa/(lambda*n),((double)1)/3)*1000;
            
        }


        private void izracunaj_obimnu_brzinu_obrtanja()
        {
            Va = PI * (motori[izabran_motor].getda() / 1000) * n / 60;
            
        }

        private void izracunaj_polni_korak_motora()
        {
            Tau = PI * motori[izabran_motor].getda() / (2000 * p);
            
            bp = Tau * alfa;
            

        }

        private void izracunaj_frekvenciju_magnecenja_rotora()
        {
            fa = p * n / 60;
            
        }

        public void izracunaj_daprim()
        {
            alfa = motori[izabran_motor].getalpha() / 180;
            c = 60 / (alfa * PI * PI * A * Bs);
            Daprim = Math.Pow(c * Pa / (lambda * n), ((double)1) / 3)*1000;
        }

        private void izracunaj_bi_min_i_max()
        {
            if (U < 110)
            {
                bi_min = 0.1;
                bi_max = 0.6;
            }
            else
            {
                bi_min = 0.6;
                bi_max = 0.8;
            }
        }

        private void izracunaj_deltapprim_min_i_max()
        {
            P = eta * Pprim; 
            double c1 = P * 9.55 / n;

            if (c1 < 0.48)
                deltapprim_min = linear(c1, 0, 0.48, 10, 8.8);
            else if (c1 < 1.3)
                deltapprim_min = linear(c1, 0.48, 1.3, 8.8, 8);
            else if (c1 < 1.85)
                deltapprim_min = linear(c1, 1.3, 1.85, 8, 7.6);
            else if (c1 < 2.6)
                deltapprim_min = linear(c1, 1.85, 2.6, 7.6, 7.5);

            if (c1 < 0.88)
                deltapprim_max = linear(c1, 0, 0.88, 13.2, 11.35);
            else if (c1 < 1.85)
                deltapprim_max = linear(c1, 0.88, 1.85, 11.35, 10.4);
            else if (c1 < 2.6)
                deltapprim_max = linear(c1, 1.85, 2.6, 10.4, 10);
        }


        private void izracunaj_deltaaprim_min_i_max()
        { 
            double c1=P*9.55/n;

            if (c1 > 2.6) MessageBox.Show("P/n van opsega grafika!");

            if (c1 < 1.19)
            {
                deltaaprim_max = -2.7310924369748 * c1 + 15;
                
            }
            else
            {
                if (c1 < 2.17)
                {
                    deltaaprim_max = -1.7857142857143 * c1 + 13.875;
                }
                else
                {
                    deltaaprim_max = 10;
                }
            
            }
            if (c1 < 1)
            {
                deltaaprim_min = -2.4 * c1 + 12;
            }
            else
            {
                if (c1 < 2.13)
                {
                    deltaaprim_min = -1.4513274336283 * c1 + 11.051327433628;
                }
                else
                {
                    deltaaprim_min = 7.96;
                }
            }


                
        }

        private void izracunaj_Bs_i_A_min_max()
        {
            P = eta * Pprim;
            double c1= P / n;

            if (c1 > 0.22) MessageBox.Show("P/n van opsega grafika!");

            if (c1 < 0.08)
            {
                Bs_min = 1.3750000000000002*c1 + 0.16;
                Bs_max = 1.5*c1 + 0.4;

            }
            else {
                Bs_min = 0.35714285714285704*c1 + 0.24142857142857146;
                Bs_max = 0.6428571428571426*c1 + 0.46857142857142864;

            }

            if (c1 < 0.023)
            {
                A_min = 278260.8695652174*c1 + 6200;
                A_max = 173913.04347826086*c1 + 16000;

            }
            else
            {
                if (c1 < 0.05)
                {
                    A_min = 125925.92592592591*c1 + 9703.703703703704;
                    A_max = 74074.07407407406 * c1 + 18296.296296296295;

                }
                else 
                {
                    if (c1<0.09)
                    {
                        A_min = 77500.00000000001*c1 + 12125;
                        A_max = 65000.00000000001*c1 + 18750;
                    }
                    else 
                    {
                        if (c1<0.16)
                        {
                            A_min = 41428.57142857143*c1 + 15371.428571428572;
                            A_max = 24600;

                        }
                        else
                        {
                            A_min = 17948.717948717953*c1 + 19128.20512820513;
                            A_max = 38461.53846153847*c1 + 18446.153846153844;
                        }
                    }
                }
            }
            //MessageBox.Show(A_min + " " + A_max + " " + Bs_min + " " + Bs_max);
            
        }


        private int parametar;
        
        private void postavi_progress_bar()
        {
            try
            {
                if (InvokeRequired) // Line #1
                {
                    this.Invoke(new MethodInvoker(postavi_progress_bar));
                    return;
                }
                button1.Enabled = false;
                progressBar1.Value = parametar;
            }
            catch (Exception) { }
        }
        private void iskljuci_progress_bar()
        {
            try
            {
                if (InvokeRequired) // Line #1
                {
                    this.Invoke(new MethodInvoker(iskljuci_progress_bar));
                    return;
                }
                textBox1.Enabled = true;
                textBox2.Enabled = true;
                textBox3.Enabled = true;
                textBox4.Enabled = true;
                textBox5.Enabled = true;
                textBox6.Enabled = true;
                textBox7.Enabled = true;
                textBox8.Enabled = true;
                textBox9.Enabled = true;
                textBox10.Enabled = true;
                textBox11.Enabled = true;
                button1.Enabled = true;
                progressBar1.Visible = false;
            }
            catch (Exception) { }
        }

        private void ukljuci_progress_bar()
        {
            try
            {
                if (InvokeRequired) // Line #1
                {
                    this.Invoke(new MethodInvoker(ukljuci_progress_bar));
                    return;
                }

                textBox1.Enabled = false;
                textBox2.Enabled = false;
                textBox3.Enabled = false;
                textBox4.Enabled = false;
                textBox5.Enabled = false;
                textBox6.Enabled = false;
                textBox7.Enabled = false;
                textBox8.Enabled = false;
                textBox9.Enabled = false;
                textBox10.Enabled = false;
                textBox11.Enabled = false;
                progressBar1.Visible = true;
            }
            catch (Exception) { }
        }

        static public double linear(double x, double x0, double x1, double y0, double y1)
        {
            if ((x1 - x0) == 0)
            {
                return (y0 + y1) / 2;
            }
            return y0 + (x - x0) * (y1 - y0) / (x1 - x0);
        }

        private void izracunaj_hp()
        {
            if (Bp < 0.33)
                Hp = linear(Bp, 0, 0.33, 0, 70);
            else if (Bp < 0.62)
                Hp = linear(Bp, 0.33, 0.62, 70, 195);
            else if (Bp < 0.9)
                Hp = linear(Bp, 0.62, 0.9, 195, 400);
            else if (Bp < 1.12)
                Hp = linear(Bp, 0.9, 1.12, 400, 655);
            else if (Bp < 1.33)
                Hp = linear(Bp, 1.12, 1.33, 655, 1080);
            else if (Bp < 1.494)
                Hp = linear(Bp, 1.33, 1.494, 1080, 1570);
            else if (Bp < 1.55)
                Hp = linear(Bp, 1.494, 1.55, 1570, 1925);
            else if (Bp < 1.58)
                Hp = linear(Bp, 1.55, 1.58, 1925, 2300);
            else Hp = linear(Bp, 1.58, 1.6, 2300, 2800);

        }

        private void izracunaj_ha()
        {
            if (Ba < 0.33)
                Ha = linear(Ba, 0, 0.33, 0, 70);
            else if (Ba < 0.62)
                Ha = linear(Ba, 0.33, 0.62, 70, 195);
            else if (Ba < 0.9)
                Ha = linear(Ba, 0.62, 0.9, 195, 400);
            else if (Ba < 1.12)
                Ha = linear(Ba, 0.9, 1.12, 400, 655);
            else if (Ba < 1.33)
                Ha = linear(Ba, 1.12, 1.33, 655, 1080);
            else if (Ba < 1.494)
                Ha = linear(Ba, 1.33, 1.494, 1080, 1570);
            else if (Ba < 1.55)
                Ha = linear(Ba, 1.494, 1.55, 1570, 1925);
            else if (Ba < 1.58)
                Ha = linear(Ba, 1.55, 1.58, 1925, 2300);
            else Ha = linear(Ba, 1.58, 1.6, 2300, 2800);

        }

        private void izracunaj_hj()
        {
            if (Bj < 0.33)
                Hj = linear(Bj, 0, 0.33, 0, 70);
            else if (Bj < 0.62)
                Hj = linear(Bj, 0.33, 0.62, 70, 195);
            else if (Bj < 0.9)
                Hj = linear(Bj, 0.62, 0.9, 195, 400);
            else if (Bj < 1.12)
                Hj = linear(Bj, 0.9, 1.12, 400, 655);
            else if (Bj < 1.33)
                Hj = linear(Bj, 1.12, 1.33, 655, 1080);
            else if (Bj < 1.494)
                Hj = linear(Bj, 1.33, 1.494, 1080, 1570);
            else if (Bj < 1.55)
                Hj = linear(Bj, 1.494, 1.55, 1570, 1925);
            else if (Bj < 1.58)
                Hj = linear(Bj, 1.55, 1.58, 1925, 2300);
            else Hj = linear(Bj, 1.58, 1.6, 2300, 2800);

        }

        private void izracunaj_hz()
        {
            if (Bz < 0.33)
                Hz = linear(Bz, 0, 0.33, 0, 70);
            else if (Bz < 0.62)
                Hz = linear(Bz, 0.33, 0.62, 70, 195);
            else if (Bz < 0.9)
                Hz = linear(Bz, 0.62, 0.9, 195, 400);
            else if (Bz < 1.12)
                Hz = linear(Bz, 0.9, 1.12, 400, 655);
            else if (Bz < 1.33)
                Hz = linear(Bz, 1.12, 1.33, 655, 1080);
            else if (Bz < 1.494)
                Hz = linear(Bz, 1.33, 1.494, 1080, 1570);
            else if (Bz < 1.55)
                Hz = linear(Bz, 1.494, 1.55, 1570, 1925);
            else if (Bz < 1.58)
                Hz = linear(Bz, 1.55, 1.58, 1925, 2300);
            else Hz = linear(Bz, 1.58, 1.6, 2300, 2800);
        
        }

        private void uslov_Bz() //Bs, motor
        {

            Bz = Bs * PI * motori[izabran_motor].getda() / (0.93 * motori[izabran_motor].getz() * motori[izabran_motor].getbz());
        }

        private double Pkrozn;

        private void uslov_Et() //lambda, Bs, motor
        {
            P = eta * Pprim;
            Pkrozn = P / n;
            Tau = PI * motori[izabran_motor].getda() / (2000 * p);
            alfa = motori[izabran_motor].getalpha() / 180;
            Fa = Bs * Tau * alfa * lambda * motori[izabran_motor].getda() / 1000;
            if (Pprim <= 1000)
            {
                Pa = ((2 + eta) / (3 * eta)) * P;
            }
            else
            {
                Pa = ((1 + eta) / (2 * eta)) * P;
            }
            I = (Pprim / (U * cosfi));
            Ea = Pa / I;
            a = m * p;
            Naprim = 60 * a * Ea / (p * n * Fa);
            N1prim = Naprim / (2 * motori[izabran_motor].getz());
            int temp = ((int)(N1prim));
            if (N1prim - temp < 0.2) N1 = temp;
            else
            {
                N1 = temp + 1;
            }
            et = Math.Sqrt(2) * PI * f * N1 * Fa;
        }

        private void uslov_Ba_Bp_Bj()
        {
            Tau = PI * motori[izabran_motor].getda() / (2000 * p);
            alfa = motori[izabran_motor].getalpha() / 180;
            Fa = Bs * Tau * alfa * lambda * motori[izabran_motor].getda() / 1000;
            ha = (motori[izabran_motor].getda() - (2 * motori[izabran_motor].gethz() + motori[izabran_motor].getdo())) / 2000;
            Ba = Fa * 1000 / (2 * 0.93 * sigma * ha * lambda * motori[izabran_motor].getda());
            Bj = (Fa * sigma / (2 * 0.95 * motori[izabran_motor].getbs() * lambda * motori[izabran_motor].getda())) * 1000000;
            Bp = Fa * sigma * 2000000 / (0.95 * lambda * motori[izabran_motor].getda() * motori[izabran_motor].getds() * alfa);
        }

        private void uslov_A2_i_A3()
        {
            P = eta * Pprim;
            Tau = PI * motori[izabran_motor].getda() / (2000 * p);
            alfa = motori[izabran_motor].getalpha() / 180;
            Fa = Bs * Tau * alfa * lambda * motori[izabran_motor].getda() / 1000;
            if (Pprim <= 1000)
            {
                Pa = ((2 + eta) / (3 * eta)) * P;
            }
            else
            {
                Pa = ((1 + eta) / (2 * eta)) * P;
            }
            I = (Pprim / (U * cosfi));
            Ea = Pa / I;
            a = m * p;
            Naprim = 60 * a * Ea / (p * n * Fa);
            N1prim = Naprim / (2 * motori[izabran_motor].getz());
            int temp = ((int)(N1prim));
            if (N1prim - temp < 0.2) N1 = temp;
            else
            {
                N1 = temp + 1;
            }
            Na = motori[izabran_motor].getz() * 2 * N1;
            A2 = Na * I / (2 * PI * motori[izabran_motor].getda() / 1000);
            A3 = 60 * Pa * 1000000000 / (n * lambda * (motori[izabran_motor].getda()) * (motori[izabran_motor].getda() * motori[izabran_motor].getda()) * PI * PI * alfa * Bs);
            deltaA2 = Math.Abs((A2 - A3) / A2) * 100;
        }

        private void uslov_A1_i_A2()
        {
            P = eta * Pprim;
            Tau = PI * motori[izabran_motor].getda() / (2000 * p);
            alfa = motori[izabran_motor].getalpha() / 180;
            Fa = Bs * Tau * alfa * lambda * motori[izabran_motor].getda() / 1000;
            if (Pprim <= 1000)
            {
                Pa = ((2 + eta) / (3 * eta)) * P;
            }
            else
            {
                Pa = ((1 + eta) / (2 * eta)) * P;
            }
            I = (Pprim / (U * cosfi));
            Ea = Pa / I;
            a = m * p;
            Naprim = 60 * a * Ea / (p * n * Fa);
            N1prim = Naprim / (2 * motori[izabran_motor].getz());
            int temp = ((int)(N1prim));
            if (N1prim - temp < 0.2) N1 = temp;
            else
            {
                N1 = temp + 1;
            }
            Na = motori[izabran_motor].getz() * 2 * N1;
            A2 = Na * I / (2 * PI * motori[izabran_motor].getda() / 1000);

            deltaA = Math.Abs((A2 - A) / A) * 100;
        }

        private void uslov_dk_da()
        {
            if (kolektori[27 - redni_broj_kolektor].D < Dkprim)
            {
                Dk = motori[izabran_motor].getda();
                return;
            }
            izabran_kolektor = redni_broj_kolektor;
            do
            {
                Dk = kolektori[izabran_kolektor++].D;
            } while (Dk < Dkprim && izabran_kolektor < 28);
            izabran_kolektor += redni_broj_kolektor - 1;
            
        }

        private void uslov_deltadprim_deltad()
        {
            if (kolektori[27 - redni_broj_kolektor].D<Dkprim)
            {
                deltad = deltadprim - 1;
                return;
            }
            

            if (nizhdadbd[17] < bdprim)
            {
                deltad = deltadprim - 1;
                return;
            }

            I = (Pprim / (U * cosfi));
            izabran_kolektor = redni_broj_kolektor;
            do
            {
                Dk = kolektori[izabran_kolektor++].D;
            } while (Dk < Dkprim && izabran_kolektor < 28 - redni_broj_kolektor);
            izabran_kolektor += redni_broj_kolektor - 1;
            

            Dk = kolektori[izabran_kolektor].D;
            Vkprim = PI * Dk * n / 60000;

            if (cetkice[35 - redni_broj_Vk].Vk < Vkprim)
            {
                deltad = deltadprim - 1;
                return;
            }

            izabran_Vk = redni_broj_Vk;
            do
            {
                Vk = cetkice[izabran_Vk++].Vk;
            } while (Vk < Vkprim && izabran_Vk < 36);
            izabran_Vk += redni_broj_Vk - 1;

            deltad = cetkice[izabran_Vk].deltad;

            Sdprim = I * 100 / (p * deltad);
            adprim = Sdprim / bdprim;
            if (nizhdadbd[17] < adprim)
            {
                deltad = deltadprim - 1;
                return;
            }
            int i = 0;

            do
            {
                ad = nizhdadbd[i++];
            } while (ad < adprim);

            i = 0;

            do
            {
                bd = nizhdadbd[i++];
            } while (bd < bdprim);


            deltad = cetkice[izabran_Vk].deltad;
            deltadprim = I * 100 / (ad * bd);
        }

        private void uslov_Va_Vkprim()
        {
            if (kolektori[27 - redni_broj_kolektor].D < Dkprim)
            {
                Va = 0;
                Vkprim = Va;
                return;
            }
            if (cetkice[35 - redni_broj_Vk].Vk < Vkprim)
            {
                Va = 0;
                Vkprim = Va;
                return;
            }



            izabran_kolektor = redni_broj_kolektor;
            do
            {
                Dk = kolektori[izabran_kolektor++].D;
            } while (Dk < Dkprim && izabran_kolektor < 28 - redni_broj_kolektor);
            izabran_kolektor += redni_broj_kolektor - 1;

            Dk = kolektori[izabran_kolektor].D;
            Vkprim = PI * Dk * n / 60000;

            izabran_Vk = redni_broj_Vk;
            do
            {
                Vk = cetkice[izabran_Vk++].Vk;
            } while (Vk < Vkprim && izabran_Vk < 36);
            izabran_Vk += redni_broj_Vk - 1;

            Vk = cetkice[izabran_Vk].Vk;

            Va = PI * (motori[izabran_motor].getda() / 1000) * n / 60;

        }

        private void uslov_bkz_tau_bp()
        {
            if (kolektori[27 - redni_broj_kolektor].D < Dkprim)
            {
                Tau = 0;
                bp = 0;
                bkz = 1;
                return;
            }
            if (nizhdadbd[17] < bdprim)
            {
                Tau = 0;
                bp = 0;
                bkz = 1;
                return;
            }

            izabran_kolektor = redni_broj_kolektor;
            do
            {
                Dk = kolektori[izabran_kolektor++].D;
            } while (Dk < Dkprim && izabran_kolektor < 28 - redni_broj_kolektor);
            izabran_kolektor += redni_broj_kolektor - 1;

            Dk = kolektori[izabran_kolektor].D;

            int i = 0;


            do
            {
                bd = nizhdadbd[i++];
            } while (bd < bdprim);

            bkprim = bd * motori[izabran_motor].getda() / Dk;

            tk = PI * Dk / motori[izabran_motor].getz();
            tkprim = tk * motori[izabran_motor].getda() / Dk;
            y1 = Math.Floor(motori[izabran_motor].getz() / (2 * p));
            y2 = y1 + levidesni * m;
            bkz = bkprim + (1 + motori[izabran_motor].getz() / 2 * p - y1 - a / p) * tkprim;
            Tau = PI * motori[izabran_motor].getda() / (2000 * p);
            alfa = motori[izabran_motor].getalpha() / 180;
            bp = Tau * alfa;

        }

        private void uslov_Er_15_ili_08()
        {
            P = eta * Pprim;
            Tau = PI * motori[izabran_motor].getda() / (2000 * p);
            alfa = motori[izabran_motor].getalpha() / 180;
            Fa = Bs * Tau * alfa * lambda * motori[izabran_motor].getda() / 1000;
            if (Pprim <= 1000)
            {
                Pa = ((2 + eta) / (3 * eta)) * P;
            }
            else
            {
                Pa = ((1 + eta) / (2 * eta)) * P;
            }
            I = (Pprim / (U * cosfi));
            Ea = Pa / I;
            a = m * p;
            Naprim = 60 * a * Ea / (p * n * Fa);
            N1prim = Naprim / (2 * motori[izabran_motor].getz());
            int temp = ((int)(N1prim));
            if (N1prim - temp < 0.2) N1 = temp;
            else
            {
                N1 = temp + 1;
            }
            Na = motori[izabran_motor].getz() * 2 * N1;
            A2 = Na * I / (2 * PI * motori[izabran_motor].getda() / 1000);
            Va = PI * (motori[izabran_motor].getda() / 1000) * n / 60;
            lambdaq = 1 / (Tau * (1 - alfa));
            eq = 2.5 * Tau * N1 * motori[izabran_motor].getda() * lambda * A2 * Va * lambdaq * 0.0000000001;

            bzkr = PI * (motori[izabran_motor].getda() - 2 * motori[izabran_motor].gethkr()) / motori[izabran_motor].getz() - motori[izabran_motor].getap();

            lambdagama = 0.6 * 2 * motori[izabran_motor].gethz() / (bzkr + motori[izabran_motor].getbz()) + 1.2 * motori[izabran_motor].getda() / (lambda * motori[izabran_motor].getda()) + 0.92 * Math.Log(PI * PI * motori[izabran_motor].getda() / (motori[izabran_motor].getap() * motori[izabran_motor].getz()), 10);

            ek = 0.4 * PI * N1 * motori[izabran_motor].getda() * lambda * A2 * Va * lambdagama * 0.0000000001;

            er = ek + eq;
        }

        private void uslov_deltaaprim_deltaa() //dkprim , motor, eta , Bs, Pt, kp, ka, lambda
        {
            if (kolektori[27 - redni_broj_kolektor].D < Dkprim)
            {
                deltaaprim = 1;
                deltaa = 2;
                return;
            }


            izabran_kolektor = redni_broj_kolektor;
            do
            {
                Dk = kolektori[izabran_kolektor++].D;
            } while (Dk < Dkprim && izabran_kolektor < 28 - redni_broj_kolektor);
            izabran_kolektor += redni_broj_kolektor - 1;


            Dk = kolektori[izabran_kolektor].D;
            Vkprim = PI * Dk * n / 60000;
            if (cetkice[35 - redni_broj_Vk].Vk < Vkprim)
            {
                deltaaprim = 1;
                deltaa = 2;
                return;
            }


            izabran_Vk = redni_broj_Vk;
            do
            {
                Vk = cetkice[izabran_Vk++].Vk;
            } while (Vk < Vkprim && izabran_Vk < 36);
            izabran_Vk += redni_broj_Vk - 1;



            P = eta * Pprim;
            Tau = PI * motori[izabran_motor].getda() / (2000 * p);
            alfa = motori[izabran_motor].getalpha() / 180;
            Fa = Bs * Tau * alfa * lambda * motori[izabran_motor].getda() / 1000;
            if (Pprim <= 1000)
            {
                Pa = ((2 + eta) / (3 * eta)) * P;
            }
            else
            {
                Pa = ((1 + eta) / (2 * eta)) * P;
            }
            I = (Pprim / (U * cosfi));
            Ea = Pa / I;
            a = m * p;
            Naprim = 60 * a * Ea / (p * n * Fa);
            N1prim = Naprim / (2 * motori[izabran_motor].getz());
            int temp = ((int)(N1prim));
            if (N1prim - temp < 0.2) N1 = temp;
            else
            {
                N1 = temp + 1;
            }
            Na = motori[izabran_motor].getz() * 2 * N1;
            A2 = Na * I / (2 * PI * motori[izabran_motor].getda() / 1000);

            Va = PI * (motori[izabran_motor].getda() / 1000) * n / 60;

            kteta = 1 + 0.004 * (85 - Teta);

            Q = 65 * Pt * (1 + 0.1 * Va);

            deltaaprim = (57 / (kteta * kp)) * (lambda / (lambda + ka)) * (Q / A2);

            Scuaprim = I / (2 * deltaaprim);
            daprim = Math.Sqrt(4 * Scuaprim / (PI));

            izabran_da = redni_broj_da;
            if (vrednostida[43 - redni_broj_da] < daprim)
            {
                deltaaprim = 1;
                deltaa = 2;
                return;
            }
            do
            {
                da = vrednostida[izabran_da++];
            } while (da < daprim && izabran_da < 44);
            izabran_da += redni_broj_da - 1;

            da = vrednostida[izabran_da];
            Scua = da * da * PI / 4;

            deltaa = I / (2 * Scua);
            deltadeltaa = Math.Abs((deltaa - deltaaprim) / deltaa) * 100;
        }
        private double deltadeltaa;
        private void uslov_Um_U_cosfiprim_cosfi()
        {
            if (kolektori[27 - redni_broj_kolektor].D < Dkprim)
            {
                Um = 2 * U;
                return;
            }
            

            izabran_kolektor = redni_broj_kolektor;
            do
            {
                Dk = kolektori[izabran_kolektor++].D;
            } while (Dk < Dkprim && izabran_kolektor < 28 - redni_broj_kolektor);
            izabran_kolektor += redni_broj_kolektor - 1;


            Dk = kolektori[izabran_kolektor].D;
            Vkprim = PI * Dk * n / 60000;
            if (cetkice[35 - redni_broj_Vk].Vk < Vkprim)
            {
                Um = 2 * U;
                return;
            }


            izabran_Vk = redni_broj_Vk;
            do
            {
                Vk = cetkice[izabran_Vk++].Vk;
            } while (Vk < Vkprim && izabran_Vk < 36);
            izabran_Vk += redni_broj_Vk - 1;



            P = eta * Pprim;
            Tau = PI * motori[izabran_motor].getda() / (2000 * p);
            alfa = motori[izabran_motor].getalpha() / 180;
            Fa = Bs * Tau * alfa * lambda * motori[izabran_motor].getda() / 1000;
            if (Pprim <= 1000)
            {
                Pa = ((2 + eta) / (3 * eta)) * P;
            }
            else
            {
                Pa = ((1 + eta) / (2 * eta)) * P;
            }
            I = (Pprim / (U * cosfi));
            Ea = Pa / I;
            a = m * p;
            Naprim = 60 * a * Ea / (p * n * Fa);
            N1prim = Naprim / (2 * motori[izabran_motor].getz());
            int temp = ((int)(N1prim));
            if (N1prim - temp < 0.2) N1 = temp;
            else
            {
                N1 = temp + 1;
            }
            Na = motori[izabran_motor].getz() * 2 * N1;
            A2 = Na * I / (2 * PI * motori[izabran_motor].getda() / 1000);

            lpr = (lambda + 1.2) * motori[izabran_motor].getda();

            Va = PI * (motori[izabran_motor].getda() / 1000) * n / 60;

            kteta = 1 + 0.004 * (85 - Teta);

            Q = 65 * Pt * (1 + 0.1 * Va);

            deltaaprim = (57 / (kteta * kp)) * (lambda / (lambda + ka)) * (Q / A2);

            Scuaprim = I / (2 * deltaaprim);
            daprim = Math.Sqrt(4 * Scuaprim / (PI));

            izabran_da = redni_broj_da;
            if (vrednostida[43-redni_broj_da] < daprim)
            {
                Um = 2 * U;
                return;
            }
            do
            {
                da = vrednostida[izabran_da++];
            } while (da < daprim && izabran_da < 44);
            izabran_da += redni_broj_da - 1;

            da = vrednostida[izabran_da];
            Scua = da * da * PI / 4;

            Ra = kteta * Na * lpr / (57 * 1000 * 4 * Scua);

            deltaUa = Ra * I;


            Scupprim = I / deltapprim;

            dpprim = Math.Sqrt(4 * Scupprim / PI);

            if (vrednostida[43-redni_broj_dp] < dpprim)
            {
                Um = 2 * U;
                return;
            }
            izabran_dp = redni_broj_dp;
            do
            {
                dp = vrednostida[izabran_dp++];
            } while (dp < dpprim && izabran_dp < 44);
            izabran_dp += redni_broj_dp - 1;

            dp = vrednostida[izabran_dp];

            Scup = dp * dp * PI / 4;

            lsrp = 2.8 * (lambda * motori[izabran_motor].getda() / 1000 + motori[izabran_motor].getb() / 1000);

            izracunaj_Np();

            Rp = 2 * lsrp * Np * kteta / (57 * Scup);

            deltaUp = Rp * I;

            deltaUd = cetkice[izabran_Vk].deltaUd;

            deltaUaprim = deltaUa + deltaUp + deltaUd;

            bzkr = PI * (motori[izabran_motor].getda() - 2 * motori[izabran_motor].gethkr()) / motori[izabran_motor].getz() - motori[izabran_motor].getap();

            lambdagama = 0.6 * 2 * motori[izabran_motor].gethz() / (bzkr + motori[izabran_motor].getbz()) + 1.2 * motori[izabran_motor].getda() / (lambda * motori[izabran_motor].getda()) + 0.92 * Math.Log(PI * PI * motori[izabran_motor].getda() / (motori[izabran_motor].getap() * motori[izabran_motor].getz()), 10);

            xagamaI = PI * f * Na * Na * 0.00000001 * lambdagama * motori[izabran_motor].getda() * lambda * I / (1000 * 2 * a * a * motori[izabran_motor].getz());

            xpgamaI = 2 * PI * f * 2 * p * Np * (sigmap - 1) * gamas * Fa;

            deltaUrprim = xagamaI + xpgamaI;

            Ep = 4.44 * f * 2 * p * Np * Math.Sqrt(2) * Fa;

            
            ks = (PI * motori[izabran_motor].getda() / motori[izabran_motor].getz() + 10 * ((motori[izabran_motor].getds() - motori[izabran_motor].getda()) / 2)) / (bzkr + 10 * ((motori[izabran_motor].getds() - motori[izabran_motor].getda()) / 2));
            
            Eq = 0.15 * f * alfa * alfa * alfa * Na * Na * motori[izabran_motor].getda() * motori[izabran_motor].getda() * lambda * I * 0.00000000000001 / (a * a * p * p * ks * ((motori[izabran_motor].getds() - motori[izabran_motor].getda()) / 2000));

            Ur = Eq + Ep + deltaUrprim;

            Ua = Ea + deltaUaprim;

            Um = Math.Sqrt(Ua * Ua + Ur * Ur);

            deltaU = Math.Abs((Um - U) / U) * 100;

            cosfiprim = Ua / Um;

            deltacosfi = Math.Abs((cosfi - cosfiprim) / cosfi) * 100;



        }

        private void uslov_eta_etaprim()
        {
            if (kolektori[27 - redni_broj_kolektor].D < Dkprim)
            {
                deltaeta = 100;
                return;
            }
            if (cetkice[35 - redni_broj_Vk].Vk < Vkprim)
            {
                deltaeta = 100;
                return;
            }
            if (nizhdadbd[17] < bdprim)
            {
                deltaeta = 100;
                return;
            }

            
            I = (Pprim / (U * cosfi));

            Scupprim = I / deltapprim;

            dpprim = Math.Sqrt(4 * Scupprim / PI);
            if (vrednostida[43 - redni_broj_dp] < dpprim)
            {
                deltaeta = 100;
                return;
            }

            


            izabran_kolektor = redni_broj_kolektor;
            do
            {
                Dk = kolektori[izabran_kolektor++].D;
            } while (Dk < Dkprim && izabran_kolektor < 28 - redni_broj_kolektor);
            izabran_kolektor += redni_broj_kolektor - 1;


            Dk = kolektori[izabran_kolektor].D;
            Vkprim = PI * Dk * n / 60000;



            izabran_Vk = redni_broj_Vk;
            do
            {
                Vk = cetkice[izabran_Vk++].Vk;
            } while (Vk < Vkprim && izabran_Vk < 36);
            izabran_Vk += redni_broj_Vk - 1;

            deltad = cetkice[izabran_Vk].deltad;
            deltaUd = cetkice[izabran_Vk].deltaUd;
            Sdprim = I * 100 / (p * deltad);
            adprim = Sdprim / bdprim;
            if (nizhdadbd[17] < adprim)
            {
                deltaeta = 100;
                return;
            }
            int i = 0;

            do
            {
                ad = nizhdadbd[i++];
            } while (ad < adprim);

            i = 0;

            do
            {
                bd = nizhdadbd[i++];
            } while (bd < bdprim);


            P = eta * Pprim;
            Tau = PI * motori[izabran_motor].getda() / (2000 * p);
            alfa = motori[izabran_motor].getalpha() / 180;
            bp = Tau * alfa;
            Fa = Bs * Tau * alfa * lambda * motori[izabran_motor].getda() / 1000;
            if (Pprim <= 1000)
            {
                Pa = ((2 + eta) / (3 * eta)) * P;
            }
            else
            {
                Pa = ((1 + eta) / (2 * eta)) * P;
            }
            
            Ea = Pa / I;
            a = m * p;
            Naprim = 60 * a * Ea / (p * n * Fa);
            N1prim = Naprim / (2 * motori[izabran_motor].getz());
            int temp = ((int)(N1prim));
            if (N1prim - temp < 0.2) N1 = temp;
            else
            {
                N1 = temp + 1;
            }
            Na = motori[izabran_motor].getz() * 2 * N1;

            A2 = Na * I / (2 * PI * motori[izabran_motor].getda() / 1000);

            Va = PI * (motori[izabran_motor].getda() / 1000) * n / 60;

            kteta = 1 + 0.004 * (85 - Teta);

            Q = 65 * Pt * (1 + 0.1 * Va);

            deltaaprim = (57 / (kteta * kp)) * (lambda / (lambda + ka)) * (Q / A2);


            Scuaprim = I / (2 * deltaaprim);
            daprim = Math.Sqrt(4 * Scuaprim / (PI));

            izabran_da = redni_broj_da;
            if (vrednostida[43 - redni_broj_da] < daprim)
            {
                deltaeta = 100;
                return;
            }


            
            
            do
            {
                da = vrednostida[izabran_da++];
            } while (da < daprim && izabran_da < 44);
            izabran_da += redni_broj_da - 1;

            Scua = da * da * PI / 4;

            lpr = (lambda + 1.2) * motori[izabran_motor].getda();

            kteta = 1 + 0.004 * (85 - Teta);

            Ra = kteta * Na * lpr / (57 * 1000 * 4 * Scua);

            
            izabran_dp = redni_broj_dp;
            do
            {
                dp = vrednostida[izabran_dp++];
            } while (dp < dpprim && izabran_dp < 44);
            izabran_dp += redni_broj_dp - 1;

            Scup = dp * dp * PI / 4;

            lsrp = 2.8 * (lambda * motori[izabran_motor].getda() / 1000 + motori[izabran_motor].getb() / 1000);

            izracunaj_Np();

            Rp = 2 * lsrp * Np * kteta / (57 * Scup);


            mi = cetkice[izabran_Vk].mi;
            Pd = cetkice[izabran_Vk].Pd;
            Vk = cetkice[izabran_Vk].Vk;


            ha = (motori[izabran_motor].getda() - (2 * motori[izabran_motor].gethz() + motori[izabran_motor].getdo())) / 2000;
            Ba = Fa * 1000 / (2 * 0.93 * sigma * ha * lambda * motori[izabran_motor].getda());
            Bj = (Fa * sigma / (2 * 0.95 * motori[izabran_motor].getbs() * lambda * motori[izabran_motor].getda())) * 1000000;
            Bp = Fa * sigma * 2000000 / (0.95 * lambda * motori[izabran_motor].getda() * motori[izabran_motor].getds() * alfa);
            Bz = Bs * PI * motori[izabran_motor].getda() / (0.93 * motori[izabran_motor].getz() * motori[izabran_motor].getbz());

            Pcua = Ra * I * I;
            Pcup = Rp * I * I;
            Pdk = deltaUd * I;
            Gp = 7700 * 2 * p * bp * motori[izabran_motor].getda() * lambda * motori[izabran_motor].gethp() / 1000000;

            Gja = 7700 * (motori[izabran_motor].getda() - 2 * motori[izabran_motor].gethz()) * (motori[izabran_motor].getda() - 2 * motori[izabran_motor].gethz()) * lambda * motori[izabran_motor].getda() * PI / 4000000000;

            Gza = 7700 * motori[izabran_motor].getz() * motori[izabran_motor].getbz() * motori[izabran_motor].gethz() * motori[izabran_motor].getda() * lambda / 1000000000;

            Gjs = 7700 * (PI * motori[izabran_motor].getdss() - 2 * motori[izabran_motor].getb()) * motori[izabran_motor].getbs() * motori[izabran_motor].getda() * lambda / 1000000000;

            izracunaj_pp_ps_pz_i_pa();

            Pp = pp * Gp * Bp * Bp * 2;
            Pja = pa * Gja * Ba * Ba * 2;
            Pza = pz * Gza * Bz * Bz * 2;
            Pjs = ps * Gjs * Bj * Bj * 2;
            Pfe = Pp + Pja + Pza + Pjs;

            Ptrd = 9.81 * mi * Pd * ad * bd * 2 * Vk / 100000000;
            Ptrl = km * (Gja + Gza) * n * 0.001;
            Ptrv = 0.3 * motori[izabran_motor].getda() * 0.001 * motori[izabran_motor].getda() * 0.001 * motori[izabran_motor].getda() * 0.001 * motori[izabran_motor].getda() * 0.001 * motori[izabran_motor].getda() * 0.001 * (1 + 5 * lambda) * n * n * n * 0.000001;
            Pmeh = Ptrd + Ptrl + Ptrv;

            Pgama = kd * (Pcua + Pcup + Pdk + Pfe + Pmeh);

            etaprim = (U * I * cosfi - Pgama) / (U * I * cosfi);

            deltaeta = Math.Abs((eta - etaprim) / eta) * 100;
        
        }

        private void uslov_da_daprim()
        {
            alfa = motori[izabran_motor].getalpha() / 180;
            c = 60 / (alfa * PI * PI * A * Bs);
            Daprim = Math.Pow(c * Pa / (lambda * n), ((double)1) / 3) * 1000;
        }
        private double dauslov;
        private void uslov_da()
        {
            alfa = motori[izabran_motor].getalpha() / 180;
            P = eta * Pprim;
            if (P < 1000)
            {
                dauslov = Math.Pow(60 * (2 + eta) * Pprim / (alfa * PI * PI * A * Bs * lambda * n * 3), ((double)1) / 3);
            }
            else
            {
                dauslov = Math.Pow(60 * (1 + eta) * Pprim / (alfa * PI * PI * A * Bs * lambda * n * 2), ((double)1) / 3);
            }
        } 

        

        private void izracunaj_eta_min_max()
        {
            if (Pprim<180)
            {
                eta_min = linear(Pprim, 0, 180, 0.327, 0.49);
                eta_max = linear(Pprim, 0, 180, 0.68, 0.81);
            }
            else if (Pprim < 390)
            {
                eta_min = linear(Pprim, 180, 390, 0.49, 0.575);
                eta_max = linear(Pprim, 180, 390, 0.81, 0.885);
            }
            else if (Pprim < 700)
            {
                eta_min = linear(Pprim, 390, 700, 0.575, 0.625);
                eta_max = linear(Pprim, 390, 700, 0.885, 0.965);
            }
            else if (Pprim < 1190)
            {
                eta_min = linear(Pprim, 700, 1190, 0.625, 0.65);
                eta_max = linear(Pprim, 700, 1190, 0.965, 0.98);
            }
            else if (Pprim < 3000)
            {
                eta_min = linear(Pprim, 1190, 3000, 0.65, 0.66);
                eta_max = linear(Pprim, 1190, 3000, 0.98, 0.99);
            }
        }

        private void izracunaj_Pcd_min_max()
        {
            if (Pprim < 400)
            {
                Pcd_min = linear(Pprim, 200, 400, 0.114, 0.112);
                Pcd_max = linear(Pprim, 200, 400, 0.133, 0.131);
            }
            else if (Pprim < 800)
            {
                Pcd_min = linear(Pprim, 400, 800, 0.112, 0.109);
                Pcd_max = linear(Pprim, 400, 800, 0.131, 0.1275);
            }
            else if (Pprim < 1300)
            {
                Pcd_min = linear(Pprim, 800, 1300, 0.109, 0.108);
                Pcd_max = linear(Pprim, 800, 1300, 0.1275, 0.125);
            }
            else if (Pprim < 3000)
            {
                Pcd_min = linear(Pprim, 1300, 3000, 0.108, 0.108);
                Pcd_max = linear(Pprim, 1300, 3000, 0.125, 0.125);
            }
        }
        private double Pa1;
        private void uslov_pa1_pa()
        {
            P = eta * Pprim;
            if (Pprim <= 1000)
            {
                Pa = ((2 + eta) / (3 * eta)) * P;
            }
            else
            {
                Pa = ((1 + eta) / (2 * eta)) * P;
            }

            Pa1 = (1 - Pcd) * Pprim;
            deltaPa = Math.Abs((Pa1 - Pa) / Pa1) * 100; 
        }
        private double deltaPa;
        private double ejd;
        private double Ea1;
        private void uslov_ea1_ea()
        {
            P = eta * Pprim;
            I = (Pprim / (U * cosfi));
            if (Pprim <= 1000)
            {
                Pa = ((2 + eta) / (3 * eta)) * P;
            }
            else
            {
                Pa = ((1 + eta) / (2 * eta)) * P;
            }
            Ea = Pa / I;
            Ea1 = (cosfi - ejd) * U;
            deltaEa = Math.Abs((Ea1 - Ea) / Ea1) * 100; 

        }
        private double deltaEa;
        private void izracunaj_ejd_min_max()
        {
            if (Pprim < 990)
            {
                ejd_min = linear(Pprim, 200, 990, 0.115, 0.112);
                ejd_max = linear(Pprim, 200, 990, 0.128, 0.125);
            }
            else
            {
                ejd_min = linear(Pprim, 990, 3000, 0.112, 0.116);
                ejd_max = linear(Pprim, 990, 3000, 0.125, 0.129);
            }
        
        }


        private void ceo_racun()
        {
            uslov_ea1_ea();

            uslov_pa1_pa();

            uslov_Bz();

            uslov_Et();

            uslov_A2_i_A3();

            uslov_Er_15_ili_08();

            uslov_da();

            uslov_A1_i_A2();

            uslov_Ba_Bp_Bj();

            uslov_dk_da();

            uslov_deltadprim_deltad();

            uslov_Va_Vkprim();

            uslov_bkz_tau_bp();

            uslov_Um_U_cosfiprim_cosfi();

            uslov_eta_etaprim();

            izracunaj_N1_Nz_i_Na();

            int i = 0;

            do
            {
                hd = nizhdadbd[i++];
            } while (hd < hdprim);


            Nzprim = Naprim / motori[izabran_motor].getz();
            c = 60 / (alfa * PI * PI * A * Bs);
            Daprim = Math.Pow(c * Pa / (lambda * n), ((double)1) / 3) * 1000;
            deltaa = I / (2 * Scua);
            l = lambda * motori[izabran_motor].getda();
            delta = (motori[izabran_motor].getds() - motori[izabran_motor].getda()) / 2;
            t = PI * motori[izabran_motor].getda() / motori[izabran_motor].getz();
            M = 9.55 * P / n;
            alfacu = Nz * Scua / motori[izabran_motor].getsz();
            kdelta=(t+10*delta)/(bzkr+10*delta);
            mdelta=800000* Bs * kdelta * 2 * delta;
            Npsekund = kn * Na;
            fa = p * n / 60;
            posle_Dk();
        }
        private double kdelta;
        private double mdelta;
        private double M;
        private double t;
        private double delta;
        private double l;


        private void svi_proracuni()
        {
            uslov_ea1_ea(); // eta -----
            
            uslov_pa1_pa(); // eta -------
            
            uslov_Bz(); // motor, Bs, eta ----------
            
            uslov_Et(); // motor, Bs, lambda, eta -----------

            uslov_A2_i_A3(); // motor, Bs, lambda, eta ----------

            uslov_Er_15_ili_08(); // motor, Bs, lambda, eta ------------

            uslov_da(); // motor, Bs, lambda, eta, A ---------------

            uslov_A1_i_A2(); // motor, Bs, Lambda, eta, A -------------

            uslov_Ba_Bp_Bj(); // motor, Bs, lambda, eta, sigma -------------
            
            uslov_dk_da(); // motor, Dkprim ------------------
            
            uslov_deltadprim_deltad(); // motor, Dkprim, bdprim, bi, hdprim

            uslov_Va_Vkprim(); // motor, Dkprim ------------

            uslov_bkz_tau_bp(); // motor, Dkprim, bdprim, bi


            uslov_Um_U_cosfiprim_cosfi(); // motor, Bs, lambda, eta, sigma, deltapprim, Pt, ka, kp, Dkprim, k, sigmap, gamas ----------------

            uslov_deltaaprim_deltaa(); // motor, Bs, lambda, eta, dkprim ,  Pt, kp, ka

            uslov_eta_etaprim(); // motor,Bs,lambda,eta,sigma,deltapprim,Pt,ka,kp,Dkprim,k,km,kd,bdprim,bi,redni_broj_kolektor,redni_broj_vk,redni_broj_da,redni_broj_dp -----------------
            // TODO proveriti da li su sve metode dobre
            
        }


        
        private void postavi_resenje_delimicno(Resenje a)
        {
            if (a.izabran_motor != -1) izabran_motor = a.izabran_motor;
            if (a.ejd != -1) ejd = a.ejd;
            if (a.Pcd != -1) Pcd = a.Pcd;
            if (a.km != -1) km = a.km;
            if (a.kd != -1) kd = a.kd;
            if (a.sigmap != -1) sigmap = a.sigmap;
            if (a.gamas != -1) gamas = a.gamas;
            if (a.hdprim != -1) hdprim = a.hdprim;
            if (a.bdprim != -1) bdprim = a.bdprim;
            if (a.redni_broj_Vk != -1) redni_broj_Vk = a.redni_broj_Vk;
            if (a.redni_broj_kolektor != -1) redni_broj_kolektor = a.redni_broj_kolektor;
            if (a.redni_broj_da != -1) redni_broj_da = a.redni_broj_da;
            if (a.redni_broj_dp != -1) redni_broj_dp = a.redni_broj_dp;
            if (a.bi != -1) bi = a.bi;
            if (a.Dkprim != -1) Dkprim = a.Dkprim;
            if (a.deltapprim != -1) deltapprim = a.deltapprim;
            if (a.k != -1) k = a.k;
            if (a.sigma != -1) sigma = a.sigma;
            if (a.eta != -1) eta = a.eta;
            if (a.A != -1) A = a.A;
            if (a.Bs != -1) Bs = a.Bs;
            if (a.lambda != -1) lambda = a.lambda;
            if (a.Pt != -1) Pt = a.Pt;
            if (a.kp != -1) kp = a.kp;
            if (a.ka != -1) ka = a.ka;
        }


        private void pronadji_kombinacije_brute()
        {
            izracunaj_eta_min_max();
            izracunaj_Pcd_min_max();
            izracunaj_ejd_min_max();
            long broj = 0;
            
            MessageBox.Show(broj.ToString());

        }

        private bool prosao_lambda_max;

        private void pronadji_kombinacije()
        {

            
            bool[] pronadjen_motor = new bool[BROJ_MOTORA];
            for (int pad = 0; pad < BROJ_MOTORA; pad++)
            {
                pronadjen_motor[pad] = false;
            }
                izracunaj_eta_min_max();
            izracunaj_Pcd_min_max();
            izracunaj_ejd_min_max();
            izracunaj_bi_min_i_max();
            izracunaj_deltapprim_min_i_max();
            long broj=0;
            int yyii=0;
            if (Pprim<200 || Pprim >3000)
            {
                MessageBox.Show("Nije pronadjen nijedan motor!");
                MessageBox.Show(Pprim.ToString());
                return;
            }
            bool ispitaj_dk = false;
            bool ispitaj_hdprim = false;
            
            ukljuci_progress_bar();
            for (eta = eta_max; eta > eta_min && !suspend; eta -= (eta_max - eta_min) / broj_seckanja_eta)
            {
                parametar = 100-(int)(((double)100 * (eta - eta_min)) / (eta_max - eta_min));
                postavi_progress_bar();
                for (ejd = ejd_min; ejd < ejd_max; ejd += (ejd_max - ejd_min) / broj_seckanja_ejd)
                {
                    uslov_ea1_ea();
                    if (Math.Abs((Ea1 - Ea) / Ea1) * 100 < 5)
                    {
                        break;
                    }
                }
                for (Pcd = Pcd_min; Pcd < Pcd_max; Pcd += (Pcd_max - Pcd_min) / broj_seckanja_Pcd)
                {
                    uslov_pa1_pa();
                    if (Math.Abs((Pa1 - Pa) / Pa1) * 100 < 5)
                    {
                        break;
                    }
                }
                if (Pcd<Pcd_max && ejd<ejd_max)
                {
                    izracunaj_Bs_i_A_min_max();
                    for (izabran_motor = 0; izabran_motor < BROJ_MOTORA && !suspend ; izabran_motor++)
                    {
                        if (pronadjen_motor[izabran_motor]) continue;
                        ispitaj_dk = false;
                        for (Dkprim = 0.5 * motori[izabran_motor].getda(); Dkprim < 0.9 * motori[izabran_motor].getda() && !suspend; Dkprim += (0.4 * motori[izabran_motor].getda()) / broj_seckanja_Dkprim)
                        {
                            uslov_Va_Vkprim();
                            uslov_dk_da();
                            if ((Dk < 0.9 * motori[izabran_motor].getda()) && (Va * 0.5 < Vkprim && Va * 0.9 > Vkprim) && (Va * 0.9 > Vk))
                            {
                                ispitaj_dk = true;
                                break;
                            }
                        }
                        for (Bs = Bs_max; Bs > Bs_min && !suspend && ispitaj_dk; Bs -= (Bs_max - Bs_min) / broj_seckanja_Bs)
                        {

                            uslov_Bz();
                            if (Bz < 1.6)
                            {
                                prosao_lambda_max = false;
                                for (lambda = 0.85; ( prosao_lambda_max==false || lambda < 0.85) && !suspend; lambda += (lambda_max - lambda_min) / broj_seckanja_lambda)
                                {
                                    if (lambda > lambda_max)
                                    {
                                        prosao_lambda_max = true;
                                        lambda = lambda_min;
                                    }
                                    uslov_Et();
                                    if (U > 30 && et >= 8)
                                    {
                                        continue;
                                    }
                                    if (U <= 30 && et >= 6)
                                    {
                                        continue;
                                    }

                                    uslov_A2_i_A3();
                                    if (Math.Abs((A2 - A3) / A2) * 100 >= 5)
                                    {
                                        continue;
                                    }
                                    uslov_Er_15_ili_08();
                                    if (U > 30 && er >= 1.5) continue;
                                    if (U <= 30 && er >= 0.5) continue;


                                    for (A = A_max; A > A_min && !suspend; A -= (A_max - A_min) / broj_seckanja_A)
                                    {
                                        uslov_A1_i_A2();

                                        if (deltaA >= 5) continue;

                                        uslov_da();
                                        if (Math.Abs((motori[izabran_motor].getda() * 0.99 / 1000 - dauslov) / dauslov) * 100 < 3)
                                        {

                                            for (sigma = sigma_min; sigma <= sigma_max && !suspend; sigma += (sigma_max - sigma_min) / broj_seckanja_sigma)
                                            {
                                                uslov_Ba_Bp_Bj();
                                                if (Ba < 1.6 && Bp < 1.6 && Bj < 1.6)
                                                {
                                                    izracunaj_deltapprim_min_i_max();

                                                    for (Dkprim = 0.5 * motori[izabran_motor].getda(); Dkprim < 0.9 * motori[izabran_motor].getda() && !suspend; Dkprim += (0.4 * motori[izabran_motor].getda()) / broj_seckanja_Dkprim)
                                                    {
                                                        uslov_Va_Vkprim();
                                                        if (!(Va * 0.5 < Vkprim && Va * 0.9 > Vkprim && Va * 0.9 > Vk)) continue;
                                                        uslov_dk_da();
                                                        if (Dk < 0.9 * motori[izabran_motor].getda())

                                                            //for (redni_broj_da = 0; redni_broj_da < 2;redni_broj_da++ )
                                                            //for (redni_broj_dp = 0; redni_broj_dp < 2; redni_broj_dp++)
                                                            //for (redni_broj_Vk = 0; redni_broj_Vk < 2; redni_broj_Vk++)
                                                            //for (redni_broj_kolektor = 0; redni_broj_kolektor < 2; redni_broj_kolektor++)
                                                            for (kp = kp_min; kp < kp_max && !suspend; kp += (kp_max - kp_min) / broj_seckanja_kp)
                                                                for (ka = ka_min; ka <= ka_max && !suspend; ka += (ka_max - ka_min) / broj_seckanja_ka)
                                                                    for (Pt = Pt_min; Pt < Pt_max && !suspend; Pt += (Pt_max - Pt_min) / broj_seckanja_Pt)
                                                                    {
                                                                        uslov_deltaaprim_deltaa();
                                                                        if (deltadeltaa >= 15) continue;
                                                                        for (k = 1.45; k < 2 && !suspend; k += 0.55 / broj_seckanja_k)
                                                                            for (deltapprim = deltapprim_min; deltapprim < deltapprim_max && !suspend; deltapprim += (deltapprim_max - deltapprim_min) / broj_seckanja_deltapprim)
                                                                                for (gamas = gamas_min; gamas < gamas_max && !suspend; gamas += (gamas_max - gamas_min) / broj_seckanja_gamas)
                                                                                    for (sigmap = sigmap_min; sigmap < sigmap_max && !suspend; sigmap += (sigmap_max - sigmap_min) / broj_seckanja_sigmap)
                                                                                    {
                                                                                        uslov_Um_U_cosfiprim_cosfi();
                                                                                        if (deltaU < 3 && deltacosfi < 3)
                                                                                        {
                                                                                            for (bi = bi_min; bi < bi_max; bi += (bi_max - bi_min) / broj_seckanja_bi)
                                                                                            {
                                                                                                izracunaj_bdprim_min_i_max();
                                                                                                for (bdprim = bdprim_min; bdprim < bdprim_max; bdprim += (bdprim_max - bdprim_min) / broj_seckanja_bdprim)
                                                                                                {
                                                                                                    uslov_bkz_tau_bp();
                                                                                                    if (bkz < 800 * (Tau - bp))
                                                                                                    {
                                                                                                        ispitaj_hdprim = false;
                                                                                                        izracunaj_hdprim_min_i_max();
                                                                                                        for (hdprim = hdprim_min; hdprim_min < hdprim_max; hdprim += (hdprim_max - hdprim_min) / broj_seckanja_hdprim)
                                                                                                        {
                                                                                                            uslov_deltadprim_deltad();
                                                                                                            if (deltadprim < deltad)
                                                                                                            {
                                                                                                                ispitaj_hdprim = true;
                                                                                                                break;
                                                                                                            }
                                                                                                        }
                                                                                                        for (kd = kd_min; kd < kd_max && !suspend && ispitaj_hdprim; kd += (kd_max - kd_min) / broj_seckanja_kd)
                                                                                                            for (km = km_min; km < km_max && !suspend; km += (km_max - km_min) / broj_seckanja_km)
                                                                                                            {
                                                                                                                uslov_eta_etaprim();
                                                                                                                if (deltaeta < 5)
                                                                                                                {
                                                                                                                    Resenje a = new Resenje();

                                                                                                                    a.Bs = Bs;
                                                                                                                    a.lambda = lambda;
                                                                                                                    a.izabran_motor = izabran_motor;
                                                                                                                    a.A = A;
                                                                                                                    a.eta = eta;
                                                                                                                    a.Pcd = Pcd;
                                                                                                                    a.ejd = ejd;
                                                                                                                    a.Dkprim = Dkprim;
                                                                                                                    a.gamas = gamas;
                                                                                                                    a.sigmap = sigmap;
                                                                                                                    a.sigma = sigma;
                                                                                                                    a.k = k;
                                                                                                                    a.deltapprim = deltapprim;
                                                                                                                    a.kp = kp;
                                                                                                                    a.Pt = Pt;
                                                                                                                    a.km = km;
                                                                                                                    a.kd = kd;
                                                                                                                    a.bi = bi;
                                                                                                                    a.hdprim = hdprim;
                                                                                                                    a.bdprim = bdprim;
                                                                                                                    a.ka = ka;
                                                                                                                    a.redni_broj_da = redni_broj_da;
                                                                                                                    a.redni_broj_dp = redni_broj_dp;
                                                                                                                    a.redni_broj_kolektor = redni_broj_kolektor;
                                                                                                                    a.redni_broj_Vk = redni_broj_Vk;
                                                                                                                    resenja[izabran_motor] = a;
                                                                                                                    //resenjatemp1.put(a);
                                                                                                                    broj++;
                                                                                                                    pronadjen_motor[izabran_motor] = true;
                                                                                                                    goto novi_motor;
                                                                                                                    //km = km_max + 1;
                                                                                                                    //kd = kd_max + 1;
                                                                                                                    //bi = bi_max + 1;
                                                                                                                    //bdprim = bdprim + 1;
                                                                                                                }
                                                                                                            }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            sigmap = sigmap_max + 1;
                                                                                            gamas = gamas_max + 1;

                                                                                        }
                                                                                    }
                                                                    }
                                                    }
                                                    
                                                }
                                            }
                                            A = A_min - 1;
                                        }
                                    }
                                }
                            }
                        }
                    novi_motor:
                        yyii++;
                    }
                }
            }


            iskljuci_progress_bar();



            


            nastavak();
        }

        private void posle_Dk()
        {
            d = kolektori[izabran_kolektor].d;
            bprim = kolektori[izabran_kolektor].b;
            lprim = kolektori[izabran_kolektor].l;
            e = kolektori[izabran_kolektor].e;
            kprim = kolektori[izabran_kolektor].k;
            cprim = kolektori[izabran_kolektor].c;
            aprim = kolektori[izabran_kolektor].a;
            tk = PI * Dk / motori[izabran_motor].getz();

            bk = tk - bi;
        
        }

        

        private void izracunaj_Np()
        {

            Tau = PI * motori[izabran_motor].getda() / (2000 * p);
            alfa = motori[izabran_motor].getalpha() / 180;
            Fa = Bs * Tau * alfa * lambda * motori[izabran_motor].getda() / 1000;

            ha = (motori[izabran_motor].getda() - (2 * motori[izabran_motor].gethz() + motori[izabran_motor].getdo())) / 2000;
            Ba = Fa * 1000 / (2 * 0.93 * sigma * ha * lambda * motori[izabran_motor].getda());
            Bj = (Fa * sigma / (2 * 0.95 * motori[izabran_motor].getbs() * lambda * motori[izabran_motor].getda())) * 1000000;
            Bp = Fa * sigma * 2000000 / (0.95 * lambda * motori[izabran_motor].getda() * motori[izabran_motor].getds() * alfa);
            
            I = (Pprim / (U * cosfi));

            izracunaj_ha();

            la = (motori[izabran_motor].getda() / 1000 - 2 * motori[izabran_motor].gethz() / 1000 - ha) * PI / (2 * p) + ha;

            Ma = Ha * la;

            lp = 1.2 * (motori[izabran_motor].getdi() - motori[izabran_motor].getds()) / 2000;

            izracunaj_hp();

            Mp = 2 * Hp * lp;

            lj = (motori[izabran_motor].getdss() - motori[izabran_motor].getbs())* Math.Asin(motori[izabran_motor].getdi()/(motori[izabran_motor].getdss()-motori[izabran_motor].getbs()))/2000;
        
            Bj = (Fa * sigma / (2*0.95*motori[izabran_motor].getbs()*lambda*motori[izabran_motor].getda()))*1000000;

            izracunaj_hj();

            Mj = 2 * Hj * lj;

            bzkr = PI * (motori[izabran_motor].getda() - 2 * motori[izabran_motor].gethkr()) / motori[izabran_motor].getz() - motori[izabran_motor].getap();

            ks = (PI * motori[izabran_motor].getda() / motori[izabran_motor].getz() + 10 * ((motori[izabran_motor].getds() - motori[izabran_motor].getda()) / 2)) / (bzkr + 10 * ((motori[izabran_motor].getds() - motori[izabran_motor].getda()) / 2));

            Ms = 800 * Bs * ks * 2 * ((motori[izabran_motor].getds() - motori[izabran_motor].getda()) / 2);

            Bz = Bs * PI * motori[izabran_motor].getda() / (0.93 * motori[izabran_motor].getz() * motori[izabran_motor].getbz());

            izracunaj_hz();

            Mz = 2 * Hz * motori[izabran_motor].gethz()/1000;

            Fp = k * (Ms + Mz + Ma + Mp + Mj);

            Npprim = Fp/(2*p*I);

            Np = Math.Ceiling(Npprim);
        }


        private void izracunaj_pp_ps_pz_i_pa()
        {
            pp = 2.6;
            ps = 2.6;
            pz = 100;
            pa = 100;
        
        }
        

        

        private void izracunaj_N1_Nz_i_Na()
        {
            int temp = ((int)(N1prim));
            if (N1prim - temp < 0.2) N1 = temp;
            else
            {
                N1 = temp + 1;
            }
            Nz = 2 * N1;
            Na = motori[izabran_motor].getz() * 2 * N1;
        }

        private void izracunaj_Aprim_i_dA()
        {
            A2 = Na * I / (2 * PI * motori[izabran_motor].getda() / 1000);

            deltaA = Math.Abs((A2 - A) / A) * 100;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Pprim = Utilities.string_to_double(textBox1.Text);
                U = Utilities.string_to_double(textBox2.Text);
                f = Utilities.string_to_double(textBox3.Text);
                n = Utilities.string_to_double(textBox4.Text);
                p = Utilities.string_to_double(textBox5.Text);
                cosfi = Utilities.string_to_double(textBox6.Text);
                if (textBox7.Text[0] == 'O' || textBox7.Text[0] == 'o')
                {
                    Pt_min = 14;
                    Pt_max = 18;
                }
                else
                {
                    Pt_min = 36;
                    Pt_max = 44;
                }
                Teta = Utilities.string_to_double(textBox8.Text);
                m = Utilities.string_to_double(textBox9.Text);
                for (int i = 0; i < BROJ_MOTORA; i++)
                    motori[i].calculatebsmax(p);

                kn = Utilities.string_to_double(textBox10.Text);

                if (textBox11.Text[0] == 'D' || textBox11.Text[0] == 'd')
                {
                    levidesni = -1;
                }
                else
                {
                    levidesni = 1;
                }


                

                radnik = new Thread(pronadji_kombinacije);
                button1.Enabled = false;
                radnik.Start();

            }
            catch (Nema_Motora e1)
            {
                MessageBox.Show(e1.ToString());
                Console.WriteLine(e1.ToString());

            }
            catch (Exception e1)
            {
                MessageBox.Show("Unesi validne vrednosti!");
                Console.WriteLine(e1.ToString());
                
            }

        }

        private void button1_Click_alt(object sender, EventArgs e)
        {

            try
            {
                Pprim = Utilities.string_to_double(textBox1.Text);
                U = Utilities.string_to_double(textBox2.Text);
                f = Utilities.string_to_double(textBox3.Text);
                n = Utilities.string_to_double(textBox4.Text);
                p = Utilities.string_to_double(textBox5.Text);
                cosfi = Utilities.string_to_double(textBox6.Text);
                if (textBox7.Text[0] == 'O' || textBox7.Text[0] == 'o')
                {
                    Pt_min = 14;
                    Pt_max = 18;
                }
                else
                {
                    Pt_min = 36;
                    Pt_max = 44;
                }
                Teta = Utilities.string_to_double(textBox8.Text);
                m = Utilities.string_to_double(textBox9.Text);
                for (int i = 0; i < BROJ_MOTORA; i++)
                    motori[i].calculatebsmax(p);

                kn = Utilities.string_to_double(textBox10.Text);

                if (textBox11.Text[0] == 'D' || textBox11.Text[0] == 'd')
                {
                    levidesni = -1;
                }
                else
                {
                    levidesni = 1;
                }

                postavi_resenje(new Resenje());

                eta = 0.6969;
                izabran_motor = 0;
                Bs = 0.29967;
                lambda = 0.64;
                A = 20067;
                Pt = 40;
                ka = 1.1;
                kp = 1.2;
                deltapprim = 11;
                Dkprim = 35.0735;
                sigma = 1.09;
                k = 2;
                redni_broj_kolektor = 0;
                redni_broj_Vk = 0;
                redni_broj_da = 0;
                redni_broj_dp = 0;
                bi = 0.7;
                bdprim = 9.222976;
                kd = 1.105;
                km = 2;

                ceo_racun();

                upisiutxt();
            }
            catch (Nema_Motora e1)
            {
                MessageBox.Show(e1.ToString());
                Console.WriteLine(e1.ToString());

            }
            catch (Exception e1)
            {
                MessageBox.Show("Unesi validne vrednosti!");
                Console.WriteLine(e1.ToString());

            }
        }



        

        private void izracunaj_bdprim_min_i_max()
        {
            izabran_kolektor = redni_broj_kolektor;
            do
            {
                Dk = kolektori[izabran_kolektor++].D;
            } while (Dk < Dkprim && izabran_kolektor < 28);
            izabran_kolektor += redni_broj_kolektor - 1;
            Dk = kolektori[izabran_kolektor].D;

            tk = PI * Dk / motori[izabran_motor].getz();

            bk = tk - bi;
            bdprim_min = bk;
            bdprim_max = 1.5 * bk;
        }

        private void izracunaj_hdprim_min_i_max()
        {
            if (kolektori[27 - redni_broj_kolektor].D < Dkprim)
            {
                hdprim_min = 0;
                hdprim_max = 0;
                return;
            }

            izabran_kolektor = redni_broj_kolektor;
            do
            {
                Dk = kolektori[izabran_kolektor++].D;
            } while (Dk < Dkprim && izabran_kolektor < 28 - redni_broj_kolektor);
            izabran_kolektor += redni_broj_kolektor - 1;


            Dk = kolektori[izabran_kolektor].D;
            Vkprim = PI * Dk * n / 60000;

            if (cetkice[35 - redni_broj_Vk].Vk < Vkprim)
            {
                hdprim_min = 0;
                hdprim_max = 0;
                return;
            }

            izabran_Vk = redni_broj_Vk;
            do
            {
                Vk = cetkice[izabran_Vk++].Vk;
            } while (Vk < Vkprim && izabran_Vk < 36);
            izabran_Vk += redni_broj_Vk - 1;

            deltad = cetkice[izabran_Vk].deltad;

            Sdprim = I * 100 / (p * deltad);
            adprim = Sdprim / bdprim;
            hdprim_min = 2 * adprim;
            hdprim_max = 3 * adprim;
        }

        private void upisiutxt()
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Text file|*.txt";
                saveFileDialog1.Title = "Sacuvaj rezultat";
                saveFileDialog1.ShowDialog();

                if (saveFileDialog1.FileName != "")
                {
                    System.IO.FileStream fs =
                       (System.IO.FileStream)saveFileDialog1.OpenFile();

                    string tempstring = @"utrosena snaga  		 P'=  [Pprim]  [W]
nominalni napon napajanja	  U= [U]  [V]
frekvencija napajanja 			  f= [f]  [Hz]
brzina obrtanja motora 	 n= [n] [min-1]
broj pari polova p= [p]
1. Struja motora
struja motora				 I= [I] [A]
2. korisna snaga motora
korisna snaga 		P= [P] [W]
stepen iskoriscenja 	eta = [eta]
3.elektromagnetna snaga
elektromagnetna snaga  Pa = [Pa] [W]
pcd = [Pcd] [%]
vrednost za proveru elektromagnetne snage Pa1 [Pa1] [W]
greska deltaPa = [deltaPa] [%]
4.elektromotorna sila rotora 
elektromotorna sila rotora Ea = [Ea] [V]
ejd = [ejd] [%]
vrednost za proveru elektromotorne sile rotora Ea1 = [Ea1] [V]
greska deltaEa = [deltaEa] [%]
odnos P/n [Pkrozn][W/m-1]
5.specificna zapremina masine 
magnetna indukcija BΔ = [Bs] [T]
gustina amper provodnika A = [AA] [A/m]
6.odredjivanje precnika i osne duzine rotora 
sacinilac lambda (0,4-2) =   [lambda]
priblizni precnik rotora Da' = [Daprim] [m]
izabrani tip motora - [motor.getime()]
precnik rotora Da = [motor.getda()] [mm]
broj zljebova rotora Z = [motor.getz()]
visina zubca rotora hz = [motor.gethz()] [mm]
visina zljeba rotora hž = [motor.gethz()] [mm]
sirina zljeba rotora az = [motor.getaz()] [mm]
sirina otvora zljeba ap = [motor.getap()][mm]
sirina zubca rotora bz = bzmin = [motor.getbz()] [mm]
bzkor = bzmin = [motor.getbz()] [mm]
povrsina zljeba rotora Sz = [motor.getsz()] [mm2]
visina krunice zubca rotora hkr = [motor.gethkr()] [mm]
sirina krunice zubca rotora bzkr = [bzkr] [mm]
osna duzina rotora l = [l] [mm]
precnik osovinice rotora D0 = [motor.getdo()] [mm]
unutrasnji precnik statora Ds = [motor.getds()] [mm]
ugao pod kojim se vidi pol statora alfastepen [motor.getalpha()] [stepen]
sirina jarma statora bs = [motor.getbs()] [mm]
sirina jezgra statora Di = [motor.getdi()] [mm]
duzina jezgra statora Dss = [motor.getdss()] [mm]
visina pola statora hp = [motor.gethp()] [mm]
sirina pola statora b = [motor.getb()] [mm]
lucna duzina pola statora bp = [bp]
sirina medjugvozdja delta = [delta] [mm]
korak ozljebljenja rotora t = [t]
7.obimna brzina obrtanja rotora 
obimna brzina obrtanja rotora Va = [Va] [m/s]
8.polni korak motora 
polni korak rotora 𝜏 = [Tau] [m]
9.frekvencija magnecenja rotora 
fa = [fa] [Hz]
10.fluks pod polom opterecene masine 
fluks u medjugvozdju masine Φa = [Fa] [Wb]
11.broj provodnika u namotaju rotora 
broj provodnika rotora Na' = [Naprim]
broj provodnika u jednom sloju zljeba N1' = [N1prim]
broj provodnika u zljebu rotora NZ' = [Nzprim]
usvojene vrednosti: 
broj provodnika u jednom sloju N1 = [N1] 
broj provodnika u zljebu rotora Nz = [Nz]
ukupan broj provodnika u namotaju rotora Na = [Na]
12.korak namotaja rotora po sekcijama i kolektoru 
prednji navojni korak y1 = [y1]
zadnji navojni korak y2 = [y2]
ukupni navojni korak y = [y]
navojni korak na kolektoru yk = [y]
13.gustina amper provodnika
gustina amper provodnika A2 = [A2] [A/m]
greska delta A = [deltaA] [%]
provera izracunate vrednosti gustine amper provodnika A3 = [A3] [A/m]
greska delta A = [deltaA2] [%] 
14.izbor gustine struje 
sacinilac odvodjenja toplote sa povrsine rotora pt = [Pt] [W/m2stepenC]
toplotna snaga po jedinici spoljne povrsine rotora Q = [Q] [W/m2]
gustina struje u rotoru delta a' = [deltaaprim] [A/mm2]
temperaturni sacinilac usled porasta temperature kteta
dozvoljena temperatura namotaja tetam = 85 [stepenC]
kp (1,2-2) = [kp]
ka (0,8-1,3) = [ka]
korisni moment M = [M] [Nm]
15.presek i precnik provodnika rotora i stvarna gustina struje 
potreban presek provodnika Scua' = [Scuaprim] [mm2]
potreban precnik provodnika da' = [daprim] [mm]
usvojena vrednost precnika provodnika da = [da] [mm]
stvarni presek provodnika Scua = [Scua] [mm2]
stvarna gustina struje u provodnicima rotora delta a = [deltaa] [A/mm2]
greska delta delta a = [deltadeltaa] [%]
16.sacinilac ispune zljebom 
αcu = [alfacu]
17.provera magnetne indukcije u zupcu rotora
Bz = [Bz] [T]
18.omski otpor namotaja rotora pri nominovnoj temperaturi
srednja duzina provodnika u namotaju rotora lpr = [lpr] [m]
omski otpor namotaja rotora Ra = [Ra] [om]
19.pad napona u namotajima rotora 
delta Ua = [deltaUa] [V]
20.pad magnetnog napona u medjugvozdju 
karterov sacinilac kdelta
pad magnetnog napona u medjugvozdju mdelta [A]
21.pad magnetnog napona u zupcima rotora 
indukcija u zupcu rotora Bz = [Bz] [T]
jacina polja u zupcu rotora Hz = [Hzz] [A/m]
pad magnetnog napona u zupcima rotora Mz = [Mz] [A]
22.pad magnetnog napona u jezgru rotora
visina jezgra rotora ha = [ha] [m]
koeficijent magnetnog rasipanja (1,02-1,2) sigma = [sigma] 
indukcija u jezgru rotora Ba = [Ba] [T]
jacina polja u jezgru rotora Ha = [Ha] [A/m]
duzina linije fluksa kroz jezgro rotora la = [la] [m]
pad magnetnog napona kroz jezgro indukta Ma = [Ma] [A]
23.pad magnetnog napona u polovima statora 
duzina linije magnetnog fluksa kroz pol lp = [lp] [m]
magnetna indukcija u polu Bp = [Bp] [T]
jacina polja u polu Hp = [Hp] [A/m]
pad magnetnog napona kroz polove statora Mp = [Mp] [A]
24.pad magnetnog napona u jezgru statora
duzina linije fluksa kroz jezgro statora lj = [lj] [m]
indukcija u jarmu statora Bj = [Bj] [T]
jacina polja u jarmu statora Hj = [Hj] [A/m]
pad magnetnog napona kroz polve statora Mj = [Mj] [A]
25.ukupno magnetnopobudna sila pobudnog para polova
koeficijent (1,45-2) k = [k]
Fp = [Fp] [A]
26.broj navojaka na jednom polu
Np' = [Npprim]
27.orijentaciona vrednost navojaka pobude 
Np'' [Npsekund]
usvaja se Np = [Np]
28.presek i precnik pobudnog namotaja
orijentacioni presek provodnika pobudnog namotaja Scup' = [Scupprim] [mm]
gustina struje pobude delta p' = [] [A/mm2]
potreban precnik provodnika dp' = [dpprim] [mm]
usvaja se precnik dp = [dp] [mm]
stvarni presek provodnika je Scup = [Scup] [mm]
29.otpor namotaja pobude i aktivni pad napona u namotaju pobude
srednja duzina namotaja pobude lsrp = [lsrp] [m]
otpor namotaja pobude Rp = [Rp] [om]
pad napona u namotaju pobude delta Up = [Up] [V]
30.odredjivanje precnika kolektora 
orijentaciona vrednost precnika kolektora DK' = [Dkprim] [m]
standardni precnik kolektora DK = [Dk] [mm]
d = [d] [mm] 
b' = [bprim] [mm]
l' = [lprim] [mm]
k' = [kprim] [mm]
c' = [cprim] [mm]
a' = [aprim] [mm]
e = [e] [mm]
31.sirina kolektorskih lamela 
korak lamela na kolektoru tk = [tk] [mm]
sirina izolacije izmedju lamele bi = [bi] [mm]
sirina kolektorske lamele bk = [bk] [mm]
32.obimna brzina kolektora 
Vk' = [Vkprim] [m/s]
33.izbor dirki-orijentacione mere
potrebna povrsina preseka dirki Sd' = [Sdprim] [mm2]
orijentaciona sirina dirke bd' = [bdprim] [mm]
orijentaciona duzina dirke ad' = [adprim] [mm]
orijentaciona visina dirke hd' = [hdprim] [mm]
34.izbor dirki-stvarne mere 
usvojeni tip kolektora i kataloske vrednosti 
oznaka kvaliteta [cetkica.oz]
visina dirke hd = [hd] [mm]
sirina dirke bd = [bd] [mm]
duzina dirke ad = [ad] [mm]
prelazni napon delta Ud = [cetkica.deltaUd] [V]
koeficijent trenja mi = [cetkica.mi] 
dozvoljena gustina struje delta d [cetkica.deltad] [A/cm2]
specificni pritisak pd [cetkica.Pd] [kg/cm2]
dozvoljena obodna brzina Vk = [cetkica.Vk] [m/s]
35.stvarna gustina struje dirke
delta d = [deltadprim] [A/cm2]
36.provera komutacije 
sirina komutacione zone bkz = [bkz] [mm]
mera magnetne provodnosti za rasuti fluks jednog navojnog dela lambda gama = [lambdagama]
mera magnetne provodnosti fluksa reakcije indukta lambda q = [lambdaq]
srednja vrednost ems usled komutacije ek = [ek][V]
ems usled reakcije indukta eq = [eq] [V]
ems er = [er] [V]
ems transformacije et = [et] [V]
37.pad napona na aktivnim otpornostima i dirkama 
delta Ua' = [DeltaUaprim] [V]
38.induktivni pad napona usled rasipnog polja
induktivni pad napona u namotaju motora IXagama = [xagamaI] [V]
koeficijent dodira rasipnog fluksa sa pobudnim namotajem (0,75-0,85) gamas = [gamas]
koeficijent rasipanja (1,08-1,12) sigmap = [sigmap]
induktivni pad napona usled rasipnog polja u namotaju pobude  IXpgama = [xpgamaI] [V]
induktivni pad napona usled rasipanja u motoru deltaUr' = [deltaUrprim] [V]
39.ems samoinudkcije u pobudnom namotaju Ep = [Ep] [V]
40.ems samoindukcije u namotaju rotora usled poprecnog fluksa reakcije indukta Eq = [Eq] [V]
41.sacinilac snage motora 
aktivna komponenta napona Ua = [Ua] [V]
reaktivna komponenta Ur = [Ur] [V]
racunska vrednost napona na krajevima motora Um = [Um] [V]
faktor snage motora cos(fi) = [cosfi]
42.gubici u bakru rotora i bakru pobuda 
gubici u bakru rotora Pcua = [Pcua] [W]
gubici u bakru namotaja pobude Pcup = [Pcup] [W]
43.prelazni gubici u kontaktu dirke-kolektor
Pdk = [Pdk] [W]
44.specificna zapremina gvozdja mfe=7700[kg/m3]
masa gvozdja polova Gp = [Gp] [kg]
masa gvozdja jezgra rotora Gja = [Gja] [kg]
masa gvozdja zupca rotora Gza = [Gza] [kg]
masa gvozdja jezgra statora Gjs = [Gjs] [kg]
gubici u gvozdju polova Pp = [Pp] [W]
gubici u gvozdju jezgra rotora Pja = [Pja] [W]
gubici u gvozdju zupca rotora Pza = [Pza] [W]
gubici u gvozdju jezgra statora  Pjs = [Pjs] [W]
ukupni magnetni gubici u motoru Pfe = [Pfe] [W]
pp=ps=2,6 [W/kgT]
pz=pa=100 [W/kgT]
45.mehanicki gubici 
gubici na trenje dirki o kolektor Ptrd = [Ptrd] [W]
gubici na trenje u lezajevima Ptrl = [Ptrl] [W]
gubici o trenje vazduha Ptrv = [Ptrv] [W]
ukupni mehanicki gubici Pmeh = [Pmeh] [W]
46.ukupni gubici u univerzalnom motoru 
Pgama = [Pgama] [W]
sacinilac koji obuhvata dodatne gubitke (1,08-1,2) kd = [kd]
47.stepen iskoriscenja motora eta' = [etaprim]
greska delta eta = [deltaeta] 
";

                    ceo_racun();
                    tempstring = tempstring.Replace("[Pprim]", Pprim.ToString());
                    tempstring = tempstring.Replace("[U]", U.ToString());
                    tempstring = tempstring.Replace("[f]", f.ToString());
                    tempstring = tempstring.Replace("[n]", n.ToString());
                    tempstring = tempstring.Replace("[p]", p.ToString());
                    tempstring = tempstring.Replace("[I]", I.ToString());
                    tempstring = tempstring.Replace("[P]", P.ToString());
                    tempstring = tempstring.Replace("[eta]", eta.ToString());
                    tempstring = tempstring.Replace("[Pa]", Pa.ToString());
                    tempstring = tempstring.Replace("[Pcd]", Pcd.ToString());
                    tempstring = tempstring.Replace("[Pa1]", Pa1.ToString());
                    tempstring = tempstring.Replace("[deltaPa]", deltaPa.ToString());
                    tempstring = tempstring.Replace("[Ea]", Ea.ToString());
                    tempstring = tempstring.Replace("[ejd]", ejd.ToString());
                    tempstring = tempstring.Replace("[Ea1]", Ea1.ToString());
                    tempstring = tempstring.Replace("[deltaEa]", deltaEa.ToString());
                    tempstring = tempstring.Replace("[Pkrozn]", Pkrozn.ToString());
                    tempstring = tempstring.Replace("[Bs]", Bs.ToString());
                    tempstring = tempstring.Replace("[AA]", A.ToString());
                    tempstring = tempstring.Replace("[lambda]", lambda.ToString());
                    tempstring = tempstring.Replace("[Daprim]", Daprim.ToString());
                    tempstring = tempstring.Replace("[motor.getime()]", motori[izabran_motor].getime());
                    tempstring = tempstring.Replace("[motor.getda()]", motori[izabran_motor].getda().ToString());
                    tempstring = tempstring.Replace("[motor.getz()]", motori[izabran_motor].getz().ToString());
                    tempstring = tempstring.Replace("[motor.gethz()]", motori[izabran_motor].gethz().ToString());
                    tempstring = tempstring.Replace("[motor.getaz()]", motori[izabran_motor].getaz().ToString());
                    tempstring = tempstring.Replace("[motor.getap()]", motori[izabran_motor].getap().ToString());
                    tempstring = tempstring.Replace("[motor.getbz()]", motori[izabran_motor].getbz().ToString());
                    tempstring = tempstring.Replace("[motor.getsz()]", motori[izabran_motor].getsz().ToString());
                    tempstring = tempstring.Replace("[motor.gethkr()]", motori[izabran_motor].gethkr().ToString());
                    tempstring = tempstring.Replace("[bzkr]", bzkr.ToString());
                    tempstring = tempstring.Replace("[l]", l.ToString());
                    tempstring = tempstring.Replace("[motor.getdo()]", motori[izabran_motor].getdo().ToString());
                    tempstring = tempstring.Replace("[motor.getds()]", motori[izabran_motor].getds().ToString());
                    tempstring = tempstring.Replace("[motor.getdss()]", motori[izabran_motor].getdss().ToString());
                    tempstring = tempstring.Replace("[motor.getalpha()]", motori[izabran_motor].getalpha().ToString());
                    tempstring = tempstring.Replace("[motor.getbs()]", motori[izabran_motor].getbs().ToString());
                    tempstring = tempstring.Replace("[motor.getdi()]", motori[izabran_motor].getdi().ToString());
                    tempstring = tempstring.Replace("[motor.gethp()]", motori[izabran_motor].gethp().ToString());
                    tempstring = tempstring.Replace("[motor.getb()]", motori[izabran_motor].getb().ToString());
                    tempstring = tempstring.Replace("[bp]", bp.ToString());
                    tempstring = tempstring.Replace("[delta]", delta.ToString());
                    tempstring = tempstring.Replace("[t]", t.ToString());
                    tempstring = tempstring.Replace("[Va]", Va.ToString());
                    tempstring = tempstring.Replace("[Tau]", Tau.ToString());
                    tempstring = tempstring.Replace("[fa]", fa.ToString());
                    tempstring = tempstring.Replace("[Fa]", Fa.ToString());
                    tempstring = tempstring.Replace("[Naprim]", Naprim.ToString());
                    tempstring = tempstring.Replace("[N1prim]", N1prim.ToString());
                    tempstring = tempstring.Replace("[Nzprim]", Nzprim.ToString());
                    tempstring = tempstring.Replace("[Na]", Na.ToString());
                    tempstring = tempstring.Replace("[N1]", N1.ToString());
                    tempstring = tempstring.Replace("[Nz]", Nz.ToString());
                    tempstring = tempstring.Replace("[y1]", y1.ToString());
                    tempstring = tempstring.Replace("[y2]", y2.ToString());
                    tempstring = tempstring.Replace("[y]", m.ToString());
                    tempstring = tempstring.Replace("[A2]", A2.ToString());
                    tempstring = tempstring.Replace("[A3]", A3.ToString());
                    tempstring = tempstring.Replace("[deltaA]", deltaA.ToString());
                    tempstring = tempstring.Replace("[deltaA2]", deltaA2.ToString());
                    tempstring = tempstring.Replace("[Pt]", Pt.ToString());
                    tempstring = tempstring.Replace("[Q]", Q.ToString());
                    tempstring = tempstring.Replace("[deltaaprim]", deltaaprim.ToString());
                    tempstring = tempstring.Replace("[kp]", kp.ToString());
                    tempstring = tempstring.Replace("[ka]", ka.ToString());
                    tempstring = tempstring.Replace("[M]", M.ToString());
                    tempstring = tempstring.Replace("[Scuaprim]", Scuaprim.ToString());
                    tempstring = tempstring.Replace("[daprim]", daprim.ToString());
                    tempstring = tempstring.Replace("[da]", da.ToString());
                    tempstring = tempstring.Replace("[Scua]", Scua.ToString());
                    tempstring = tempstring.Replace("[deltaa]", deltaa.ToString());
                    tempstring = tempstring.Replace("[deltadeltaa]", deltadeltaa.ToString());
                    tempstring = tempstring.Replace("[alfacu]", alfacu.ToString());
                    tempstring = tempstring.Replace("[Bz]", Bz.ToString());
                    tempstring = tempstring.Replace("[lpr]", lpr.ToString());
                    tempstring = tempstring.Replace("[Ra]", Ra.ToString());
                    tempstring = tempstring.Replace("[deltaUa]", deltaUa.ToString());
                    tempstring = tempstring.Replace("[Bz]", Bz.ToString());
                    tempstring = tempstring.Replace("[Hzz]", Hz.ToString());
                    tempstring = tempstring.Replace("[Mz]", Mz.ToString());
                    tempstring = tempstring.Replace("[kdelta]", kdelta.ToString());
                    tempstring = tempstring.Replace("[mdelta]", mdelta.ToString());
                    tempstring = tempstring.Replace("[ha]", ha.ToString());
                    tempstring = tempstring.Replace("[sigma]", sigma.ToString());
                    tempstring = tempstring.Replace("[Ba]", Ba.ToString());
                    tempstring = tempstring.Replace("[Ha]", Ha.ToString());
                    tempstring = tempstring.Replace("[la]", la.ToString());
                    tempstring = tempstring.Replace("[Ma]", Ma.ToString());
                    tempstring = tempstring.Replace("[lp]", lp.ToString());
                    tempstring = tempstring.Replace("[Bp]", Bp.ToString());
                    tempstring = tempstring.Replace("[Hp]", Hp.ToString());
                    tempstring = tempstring.Replace("[Mp]", Mp.ToString());
                    tempstring = tempstring.Replace("[lj]", lj.ToString());
                    tempstring = tempstring.Replace("[Bj]", Bj.ToString());
                    tempstring = tempstring.Replace("[Mj]", Mj.ToString());
                    tempstring = tempstring.Replace("[Hj]", Hj.ToString());
                    tempstring = tempstring.Replace("[k]", k.ToString());
                    tempstring = tempstring.Replace("[Fp]", Fp.ToString());
                    tempstring = tempstring.Replace("[Npprim]", Npprim.ToString());
                    tempstring = tempstring.Replace("[Npsekund]", Npsekund.ToString());
                    tempstring = tempstring.Replace("[Np]", Np.ToString());
                    tempstring = tempstring.Replace("[Scupprim]", Scupprim.ToString());
                    tempstring = tempstring.Replace("[deltapprim]", deltapprim.ToString());
                    tempstring = tempstring.Replace("[dpprim]", dpprim.ToString());
                    tempstring = tempstring.Replace("[dp]", dp.ToString());
                    tempstring = tempstring.Replace("[Scup]", Scup.ToString());
                    tempstring = tempstring.Replace("[lsrp]", lsrp.ToString());
                    tempstring = tempstring.Replace("[Rp]", Rp.ToString());
                    tempstring = tempstring.Replace("[deltaUp]", deltaUp.ToString());
                    tempstring = tempstring.Replace("[Dkprim]", Dkprim.ToString());
                    tempstring = tempstring.Replace("[Dk]", Dk.ToString());
                    tempstring = tempstring.Replace("[d]", d.ToString());
                    tempstring = tempstring.Replace("[bprim]", bprim.ToString());
                    tempstring = tempstring.Replace("[lprim]", lprim.ToString());
                    tempstring = tempstring.Replace("[kprim]", kprim.ToString());
                    tempstring = tempstring.Replace("[cprim]", cprim.ToString());
                    tempstring = tempstring.Replace("[aprim]", aprim.ToString());
                    tempstring = tempstring.Replace("[e]", e.ToString());
                    tempstring = tempstring.Replace("[tk]", tk.ToString());
                    tempstring = tempstring.Replace("[bi]", bi.ToString());
                    tempstring = tempstring.Replace("[bk]", bk.ToString());
                    tempstring = tempstring.Replace("[Vkprim]", Vkprim.ToString());
                    tempstring = tempstring.Replace("[Sdprim]", Sdprim.ToString());
                    tempstring = tempstring.Replace("[bdprim]", bdprim.ToString());
                    tempstring = tempstring.Replace("[adprim]", adprim.ToString());
                    tempstring = tempstring.Replace("[hdprim]", hdprim.ToString());
                    tempstring = tempstring.Replace("[bd]", bd.ToString());
                    tempstring = tempstring.Replace("[ad]", ad.ToString());
                    tempstring = tempstring.Replace("[hd]", hd.ToString());
                    tempstring = tempstring.Replace("[cetkica.oz]", cetkice[izabran_Vk].Oz.ToString());
                    tempstring = tempstring.Replace("[cetkica.deltaUd]", cetkice[izabran_Vk].deltaUd.ToString());
                    tempstring = tempstring.Replace("[cetkica.mi]", cetkice[izabran_Vk].mi.ToString());
                    tempstring = tempstring.Replace("[cetkica.deltad]", cetkice[izabran_Vk].deltad.ToString());
                    tempstring = tempstring.Replace("[cetkica.Pd]", cetkice[izabran_Vk].Pd.ToString());
                    tempstring = tempstring.Replace("[cetkica.Vk]", cetkice[izabran_Vk].Vk.ToString());
                    tempstring = tempstring.Replace("[deltadprim]", deltadprim.ToString());
                    tempstring = tempstring.Replace("[bkz]", bkz.ToString());
                    tempstring = tempstring.Replace("[lambdagama]", lambdagama.ToString());
                    tempstring = tempstring.Replace("[lambdaq]", lambdaq.ToString());
                    tempstring = tempstring.Replace("[lambdagama]", lambdagama.ToString());
                    tempstring = tempstring.Replace("[ek]", ek.ToString());
                    tempstring = tempstring.Replace("[eq]", eq.ToString());
                    tempstring = tempstring.Replace("[er]", er.ToString());
                    tempstring = tempstring.Replace("[et]", et.ToString());
                    tempstring = tempstring.Replace("[deltaUaprim]", deltaUaprim.ToString());
                    tempstring = tempstring.Replace("[xagamaI]", xagamaI.ToString());
                    tempstring = tempstring.Replace("[gamas]", gamas.ToString());
                    tempstring = tempstring.Replace("[sigmap]", sigmap.ToString());
                    tempstring = tempstring.Replace("[xpgamaI]", xpgamaI.ToString());
                    tempstring = tempstring.Replace("[deltaUrprim]", deltaUrprim.ToString());
                    tempstring = tempstring.Replace("[Ep]", Ep.ToString());
                    tempstring = tempstring.Replace("[Eq]", Eq.ToString());
                    tempstring = tempstring.Replace("[Ua]", Ua.ToString());
                    tempstring = tempstring.Replace("[Ur]", Ur.ToString());
                    tempstring = tempstring.Replace("[Um]", Um.ToString());
                    tempstring = tempstring.Replace("[cosfi]", cosfi.ToString());
                    tempstring = tempstring.Replace("[Pcua]", Pcua.ToString());
                    tempstring = tempstring.Replace("[Pcup]", Pcup.ToString());
                    tempstring = tempstring.Replace("[Pdk]", Pdk.ToString());
                    tempstring = tempstring.Replace("[Gp]", Gp.ToString());
                    tempstring = tempstring.Replace("[Gja]", Gja.ToString());
                    tempstring = tempstring.Replace("[Gza]", Gza.ToString());
                    tempstring = tempstring.Replace("[Gjs]", Gjs.ToString());
                    tempstring = tempstring.Replace("[Pp]", Pp.ToString());
                    tempstring = tempstring.Replace("[Pja]", Pja.ToString());
                    tempstring = tempstring.Replace("[Pza]", Pza.ToString());
                    tempstring = tempstring.Replace("[Pjs]", Pjs.ToString());
                    tempstring = tempstring.Replace("[Pfe]", Pfe.ToString());
                    tempstring = tempstring.Replace("[Ptrd]", Ptrd.ToString());
                    tempstring = tempstring.Replace("[Ptrl]", Ptrl.ToString());
                    tempstring = tempstring.Replace("[Ptrv]", Ptrv.ToString());
                    tempstring = tempstring.Replace("[Pmeh]", Pmeh.ToString());
                    tempstring = tempstring.Replace("[Pgama]", Pgama.ToString());
                    tempstring = tempstring.Replace("[kd]", kd.ToString());
                    tempstring = tempstring.Replace("[etaprim]", etaprim.ToString());
                    tempstring = tempstring.Replace("[deltaeta]", deltaeta.ToString());
                    UnicodeEncoding uniEncoding = new UnicodeEncoding();

                    fs.Write(uniEncoding.GetBytes(tempstring), 0, uniEncoding.GetByteCount(tempstring));



                    fs.Close();
                }

            

        }


        private void nastavak()
        {
            if (!suspend)
            {
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(nastavak));
                    return;
                }
                int broj_motora = izracunaj_broj_motora();

                if (broj_motora == 0)
                {
                    MessageBox.Show("Nije pronadjen nijedan motor!");
                    button1.Enabled = true;
                    return;
                }
                izaberi_motor(broj_motora);

                upisiutxt();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            suspend = true;
        }



    }
}
