﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motori
{
    
    public static class Utilities
    {
        

        public static double min(double a, double b)
        {
            if (a < b) return a;
            return b;
        }
        public static double max(double a, double b)
        {
            if (a > b) return a;
            return b;
        }

        public static double string_to_double(string a)
        {
            if (a.IndexOf(",") != -1)
            {
                a = a.Replace(',', '.');
            }

            return double.Parse(a, System.Globalization.CultureInfo.InvariantCulture);
        }



    }
}
