﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motori
{
    public class Cetkica
    {
        public string Oz;
        public double mi;
        public double deltaUd;
        public double deltad;
        public double Pd;
        public double ST;
        public double SO;
        public double T;
        public double Vk;
        public string KG;

        public Cetkica(string KG, string Oz, double T, double SO, double ST, double Pd, double mi, double deltaUd,double deltad, double Vk)
        {
            this.KG = KG;
            this.mi = mi;
            this.deltaUd = deltaUd;
            this.deltad = deltad;
            this.Pd = Pd;
            this.ST = ST;
            this.SO = SO;
            this.T = T;
            this.Oz = Oz;
            this.Vk = Vk;
        }
    }
}
